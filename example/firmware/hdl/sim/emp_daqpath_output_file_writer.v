// Rev 26/04/2022 - Output txt file writer entity

`timescale 1ns / 1ps

module emp_daqpath_output_file_writer(clk, data, data_dv, idnw, idnw_dv);

   input wire clk; 
   input wire [63:0] data;
   input wire data_dv;
   input wire [31:0] idnw;
   input wire idnw_dv;

   integer 	     outfile, r;
 
   initial
    begin
		  outfile=$fopen("Data_out_HW.txt","w");
		  #10
		  $fclose(outfile);
    end
		
   always @(posedge clk) #1
    if (data_dv) 
      begin
        outfile=$fopen("Data_out_HW.txt","a");
        $fwrite(outfile, "%h\n", data);
        $fclose(outfile);
      end 
    else
    if (idnw_dv) 
      begin
        outfile=$fopen("Data_out_HW.txt","a");
        $fwrite(outfile, "#%h\n", idnw);
        $fclose(outfile);
      end 
                                  
endmodule
