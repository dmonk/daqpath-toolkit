-- Address decode logic for ipbus fabric
-- 
-- This file has been AUTOGENERATED from the address table - do not hand edit
-- 
-- We assume the synthesis tool is clever enough to recognise exclusive conditions
-- in the if statement.
-- 
-- Dave Newbold, February 2011

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package ipbus_decode_emp_daqpath_ex_design is

  constant IPBUS_SEL_WIDTH: positive := 3;
  subtype ipbus_sel_t is std_logic_vector(IPBUS_SEL_WIDTH - 1 downto 0);
  function ipbus_sel_emp_daqpath_ex_design(addr : in std_logic_vector(31 downto 0)) return ipbus_sel_t;

-- START automatically generated VHDL (Fri Sep 16 17:19:45 2022)
  constant N_SLV_MAIN_CSR: integer := 0;
  constant N_SLV_DAQPATH_CSR: integer := 1;
  constant N_SLV_CH_CSR: integer := 2;
  constant N_SLV_IN: integer := 3;
  constant N_SLV_OUT: integer := 4;
  constant N_SLAVES: integer := 5;
-- END automatically generated VHDL

    
end ipbus_decode_emp_daqpath_ex_design;

package body ipbus_decode_emp_daqpath_ex_design is

  function ipbus_sel_emp_daqpath_ex_design(addr : in std_logic_vector(31 downto 0)) return ipbus_sel_t is
    variable sel: ipbus_sel_t;
  begin

-- START automatically generated VHDL (Fri Sep 16 17:19:45 2022)
    if    std_match(addr, "----------00---------------0----") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_MAIN_CSR, IPBUS_SEL_WIDTH)); -- MAIN_CSR / base 0x00000000 / mask 0x00300010
    elsif std_match(addr, "----------00---------------1----") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_DAQPATH_CSR, IPBUS_SEL_WIDTH)); -- daqpath_csr / base 0x00000010 / mask 0x00300010
    elsif std_match(addr, "----------01--------------------") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_CH_CSR, IPBUS_SEL_WIDTH)); -- CH_CSR / base 0x00100000 / mask 0x00300000
    elsif std_match(addr, "----------10--------------------") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_IN, IPBUS_SEL_WIDTH)); -- IN / base 0x00200000 / mask 0x00300000
    elsif std_match(addr, "----------11--------------------") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_OUT, IPBUS_SEL_WIDTH)); -- OUT / base 0x00300000 / mask 0x00300000
-- END automatically generated VHDL

    else
        sel := ipbus_sel_t(to_unsigned(N_SLAVES, IPBUS_SEL_WIDTH));
    end if;

    return sel;

  end function ipbus_sel_emp_daqpath_ex_design;

end ipbus_decode_emp_daqpath_ex_design;

