-- Channel FSM for reading from input fifos when triggered 
-- Rev 26/04/2022

library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use work.emp_daqpath_Types_Package.ALL;
use work.emp_daqpath_user_package.ALL;

entity emp_daqpath_channel_fsm is

  port (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    hdr_en : IN STD_LOGIC;
    mask : IN STD_LOGIC;
    token_in : IN STD_LOGIC;
    current_id : IN STD_LOGIC_VECTOR(15 downto 0);
    nw : IN STD_LOGIC_VECTOR(15 downto 0);
    ev_id : IN STD_LOGIC_VECTOR(15 downto 0);
    token_out : OUT STD_LOGIC;
    id_fifo_re : OUT STD_LOGIC;
    nw_fifo_re : OUT STD_LOGIC;
    data_fifo_re : OUT STD_LOGIC;
    data_fifo_we : OUT STD_LOGIC;
    data_sel : OUT STD_LOGIC;
    hdr_sel : OUT STD_LOGIC_VECTOR(1 downto 0);
    err_sel : OUT STD_LOGIC_VECTOR(1 downto 0)
  );
  
end emp_daqpath_channel_fsm;


ARCHITECTURE emp_daqpath_channel_fsm_arch of emp_daqpath_channel_fsm is


type state_type is (S_IDLE, S_HEADER, S_HEADER2, S_READ, S_LAST_READ, S_ERROR, S_ERROR2, S_SKIP);
signal cs: state_type;
signal ns: state_type;

signal cnt , nw_int: integer;
signal cnt_en, cnt_rst : std_logic;

begin

nw_int <= TO_INTEGER(unsigned(nw));

cnt_proc: process (cnt,cnt_en,cnt_rst,rst,clk)
  begin
  if (rst = '1') then 
    cnt <= 1;
  elsif (cnt_en = '1') and rising_edge(clk) then
    if (cnt_rst = '1') then
      cnt <= 1;
    else
      cnt <= cnt + 1;
    end if;
  end if;
          
  end process;

FSM_regs: process (ns,rst,clk)
  begin
    if (rst = '1') then
      cs <= S_IDLE;
    elsif rising_edge(clk) then
      cs <= ns;
    end if;
  end process;

FSM_logic: process (token_in,current_id,ev_id,cnt,nw_int,cs,mask, hdr_en)
  begin
    case cs is
    
      when S_IDLE => 

  token_out <= '0';  
  cnt_en <= '0';
  cnt_rst <= '0';
  id_fifo_re <= '0';
  nw_fifo_re <= '0';
  data_fifo_re <= '0';
  data_fifo_we <= '0';
  data_sel <= '0';
  hdr_sel <= "00";
  err_sel <= "00";
    
    if (token_in = '1') then
      if (mask = '0') then
        if (current_id = ev_id) then
          if(hdr_en = '1') then
            ns <= S_HEADER;
          else
            if (nw_int = 1) then        
              ns <= S_LAST_READ;
            else
              ns <= S_READ;
            end if;
          end if;
        else
            ns <= S_ERROR;
        end if;
      else
        ns <= S_SKIP;
      end if;
    else
      ns <= S_IDLE;
    end if; 
      

      when S_HEADER => 

  token_out <= '0';  
  cnt_en <= '0';
  cnt_rst <= '0';
  id_fifo_re <= '0';
  nw_fifo_re <= '0';
  data_fifo_re <= '0';
  data_fifo_we <= '1';
  data_sel <= '0';
  err_sel <= "00";
  
    if (DW_BYTES > 1) then
      hdr_sel <= "11";
      if (nw_int = 1) then        
        ns <= S_LAST_READ;
      else
        ns <= S_READ;
      end if;
    else
       hdr_sel <= "01";
       ns <= S_HEADER2;
    end if;
    
      when S_HEADER2 => 

  token_out <= '0';  
  cnt_en <= '0';
  cnt_rst <= '0';
  id_fifo_re <= '0';
  nw_fifo_re <= '0';
  data_fifo_re <= '0';
  data_fifo_we <= '1';
  data_sel <= '0';
  hdr_sel <= "10";
  err_sel <= "00";
  
    if (nw_int = 1) then
        ns <= S_LAST_READ;
    else
        ns <= S_READ;
    end if;
        

      when S_READ =>
  
  token_out <= '0';  
  cnt_en <= '1';
  cnt_rst <= '0';
  id_fifo_re <= '0';
  nw_fifo_re <= '0';
  data_fifo_re <= '1';
  data_fifo_we <= '1';
  data_sel <= '1';
  hdr_sel <= "00";
  err_sel <= "00";

    if (cnt = nw_int -1) then
      ns <= S_LAST_READ;
    else
      ns <= S_READ;
    end if; 
    
      when S_LAST_READ =>

  token_out <= '1';  
  cnt_en <= '1';
  cnt_rst <= '1';
  id_fifo_re <= '1';
  nw_fifo_re <= '1';
  data_fifo_re <= '1';
  data_fifo_we <= '1';
  data_sel <= '1';
  hdr_sel <= "00";
  err_sel <= "00";


    ns <= S_IDLE;
  
      when S_ERROR =>
  
  cnt_en <= '1';
  cnt_rst <= '1';
  id_fifo_re <= '0';
  nw_fifo_re <= '0';
  data_fifo_re <= '0';
  data_fifo_we <= '1';
  data_sel <= '0';
  hdr_sel <= "00";

    if (DW_BYTES > 1) then
      err_sel <= "11";
      token_out <= '1';
      ns <= S_IDLE;
    else
      err_sel <= "01";
      token_out <= '0';
      ns <= S_ERROR2;
    end if;
    
      when S_ERROR2 =>

  token_out <= '1';  
  cnt_en <= '1';
  cnt_rst <= '1';
  id_fifo_re <= '0';
  nw_fifo_re <= '0';
  data_fifo_re <= '0';
  data_fifo_we <= '1';
  data_sel <= '0';
  hdr_sel <= "00";
  err_sel <= "10";

    ns <= S_IDLE;
    
      when S_SKIP =>  

  token_out <= '1';  
  cnt_en <= '0';
  cnt_rst <= '0';
  id_fifo_re <= '0';
  nw_fifo_re <= '0';
  data_fifo_re <= '0';
  data_fifo_we <= '0';
  data_sel <= '0';
  hdr_sel <= "00";
  err_sel <= "00";

    ns <= S_IDLE;

      when others =>  

  token_out <= '0';  
  cnt_en <= '0';
  cnt_rst <= '0';
  id_fifo_re <= '0';
  nw_fifo_re <= '0';
  data_fifo_re <= '0';
  data_fifo_we <= '0';
  data_sel <= '0';
  hdr_sel <= "00";
  err_sel <= "00";

    ns <= S_IDLE;      
    
      end case;

  end process;
      
end emp_daqpath_channel_fsm_arch;
