-- FSM for trigger signal transmission for internal evt_generator block
-- Rev - 26/04/2022

library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity emp_daqpath_trggen_fsm is

  port (
    clk : IN STD_LOGIC;
    en : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    delta_t : IN STD_LOGIC_VECTOR(31 downto 0);
    trg : OUT STD_LOGIC
  );

end emp_daqpath_trggen_fsm;


ARCHITECTURE emp_daqpath_trggen_fsm_arch of emp_daqpath_trggen_fsm is

type state_type is (S_IDLE, S_TRG, S_WAIT);
signal cs: state_type;
signal ns: state_type;

signal cur_ts_cnt : integer := 0;
signal current_ts : std_logic_vector(31 downto 0);

begin

cnt_proc : process (cur_ts_cnt,cs,clk,rst)
begin
  if (rst = '1') then
    cur_ts_cnt <= 1;
  elsif rising_edge(clk) then
    if (cs = S_IDLE) then
      cur_ts_cnt <= 1;
    elsif (cs = S_WAIT) then
      cur_ts_cnt <= cur_ts_cnt + 1;
    else
      cur_ts_cnt <= cur_ts_cnt;
    end if;
  end if;
end process;

current_ts <= std_logic_vector(to_unsigned(cur_ts_cnt, 32));

FSM_regs: process (ns,rst,en,clk)
  begin
    if (rst = '1') then
      cs <= S_IDLE;
    elsif rising_edge(clk) and (en = '1') then
      cs <= ns;
    end if;
end process;

FSM_logic: process (delta_t,current_ts,en,cs)
  begin
    case cs is
    
      when S_IDLE =>  

    trg <= '0';
    if (en = '1') then
        ns <= S_TRG;
    else
        ns <= S_IDLE;
    end if;  

      when S_TRG =>

    trg <= '1';
    ns <= S_WAIT;
    
      when S_WAIT =>

    trg <= '0';
    if (current_ts = delta_t) then
        ns <= S_IDLE;
    else
        ns <= S_WAIT;
    end if;

      when others =>  

    trg <= '0';
    ns <= S_IDLE;

    end case;
end process;
      
end emp_daqpath_trggen_fsm_arch;