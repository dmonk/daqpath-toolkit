-- FSM for formatting and transmitting data to slink channel
-- Rev - 26/04/2022

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity emp_daqpath_slink_transmitter_fsm is

  Port ( 
    clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    en : in STD_LOGIC;
    pause : in STD_LOGIC;
    empty : in STD_LOGIC;
    slink_rdata : in STD_LOGIC_VECTOR (63 downto 0);
    idnw_rdata : in STD_LOGIC_VECTOR (31 downto 0);
    slink_re : out STD_LOGIC;
    slink_stream_dv : out STD_LOGIC;
    slink_stream_start : out STD_LOGIC;
    slink_stream_end : out STD_LOGIC;
    idnw_re : out STD_LOGIC
  );

end emp_daqpath_slink_transmitter_fsm;

architecture emp_daqpath_slink_transmitter_fsm_arch of emp_daqpath_slink_transmitter_fsm is

type state_type is (S_IDLE, S_READ, S_LAST_READ);
signal cs: state_type;
signal ns: state_type;
signal cnt , nw_int: integer;
signal cnt_en, cnt_rst : std_logic;
signal nw : std_logic_vector(15 downto 0);
signal slink_rdata_int : std_logic_vector(63 downto 0);

begin

nw <= idnw_rdata(15 downto 0);
nw_int <= TO_INTEGER(unsigned(nw));
slink_rdata_int <= slink_rdata;

cnt_proc: process (cnt,cnt_en,cnt_rst,rst,clk)
  begin
  if (rst = '1') then 
    cnt <= 1;
  elsif (cnt_en = '1') and rising_edge(clk) then
    if (cnt_rst = '1') then
      cnt <= 1;
    else
      cnt <= cnt + 1;
            end if;
  end if;
end process;

FSM_regs: process (ns,rst,clk)
  begin
    if (rst = '1') then
      cs <= S_IDLE;
    elsif rising_edge(clk) then
      cs <= ns;
    end if;
end process;

FSM_logic: process (empty,pause,en,cnt,nw_int,cs)
  begin
    case cs is
    
      when S_IDLE => 

  cnt_en <= '0';
  cnt_rst <= '0';
  idnw_re <= '0';
  slink_re <= '0';
  slink_stream_dv <= '0';
  slink_stream_start <= '0';
  slink_stream_end <= '0';

  if (en = '1') and (pause = '0') and (empty = '0') then
    if (nw_int = 1) then
      ns <= S_LAST_READ;
    else
      ns <= S_READ;
    end if;
  else
    ns <= S_IDLE;
  end if;        

      when S_READ =>
  
  cnt_en <= not pause;
  cnt_rst <= '0';
  idnw_re <= '0';
  slink_re <= not pause;
  slink_stream_dv <= not pause;
  slink_stream_end <= '0';
  if (cnt = 1) then
    slink_stream_start <= not pause;
  else
    slink_stream_start <= '0';
  end if;

  if (pause = '0') and (cnt = nw_int -1) then
    ns <= S_LAST_READ;
  else
    ns <= S_READ;
  end if; 
    
      when S_LAST_READ =>

  cnt_en <= not pause;
  cnt_rst <=not pause;
  idnw_re <= not pause;
  slink_re <= not pause;
  slink_stream_dv <= not pause;
  slink_stream_start <= '0';
  slink_stream_end <= not pause;

 if (pause = '0') then
  ns <= S_IDLE;
 else
   ns <= S_LAST_READ;
 end if;

      when others =>  

  cnt_en <= '0';
  cnt_rst <= '0';
  idnw_re <= '0';
  slink_re <= '0';
  slink_stream_dv <= '0';
  slink_stream_start <= '0';
  slink_stream_end <= '0';


  ns <= S_IDLE;      
    
      end case;

  end process;


end emp_daqpath_slink_transmitter_fsm_arch;