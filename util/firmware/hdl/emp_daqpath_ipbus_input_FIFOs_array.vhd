-- IPBUS input fifos array block
-- Rev - 26/04/2022

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.emp_daqpath_types_package.ALL;
use work.ipbus.all;
use work.ipbus_reg_types.all;

entity emp_daqpath_ipbus_input_FIFOs_array is

  generic ( NUM_CHANNELS : integer := 16);
  
  port ( 
    ipb_clk : in std_logic;
    rx_clk : in std_logic;
    rst : in std_logic;
    en : in std_logic;
    ipb_in: in ipb_wbus;
    ipb_out: out ipb_rbus;
    id_fifo_dout : out ctrl_fifo_data_array(NUM_CHANNELS-1 downto 0);
    nw_fifo_dout : out ctrl_fifo_data_array(NUM_CHANNELS-1 downto 0);
    word_fifo_dout : out data_fifo_data_array(NUM_CHANNELS-1 downto 0);
    id_fifo_we : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    nw_fifo_we : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    word_fifo_we : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    id_fifo_empty : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    nw_fifo_empty : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    word_fifo_empty : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    id_fifo_full : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    nw_fifo_full : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    word_fifo_full : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0)
  );

end emp_daqpath_ipbus_input_FIFOs_array;

architecture emp_daqpath_ipbus_input_FIFOs_array_Arch of emp_daqpath_ipbus_input_FIFOs_array is

signal    ipb_to_slaves : ipb_wbus_array(NUM_CHANNELS-1 downto 0);
signal    ipb_from_slaves : ipb_rbus_array(NUM_CHANNELS-1 downto 0); 

signal    int_id_fifo_dout : ctrl_fifo_data_array(NUM_CHANNELS-1 downto 0);
signal    ts_fifo_rdata : data_fifo_data32_array(NUM_CHANNELS-1 downto 0);
signal    ts_fifo_re : std_logic_vector(NUM_CHANNELS-1 downto 0);
--signal    ts_fifo_empty : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    ts_fifo_full : std_logic_vector(NUM_CHANNELS-1 downto 0);

signal    id_fifo_re : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    nw_fifo_re : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    word_fifo_re : std_logic_vector(NUM_CHANNELS-1 downto 0);

signal    int_id_fifo_we, int_nw_fifo_we, int_word_fifo_we : std_logic_vector(NUM_CHANNELS-1 downto 0);

signal    int_nw_fifo_dout, int_nw_fifo_dout_reg : ctrl_fifo_data_array(NUM_CHANNELS-1 downto 0);
signal    int_word_fifo_dout : data_fifo_data_array(NUM_CHANNELS-1 downto 0);
signal    fsm_empty, dw_fifo_empty_int, nw_fifo_empty_int, id_fifo_empty_int, ts_fifo_empty_int  : std_logic_vector(NUM_CHANNELS-1 downto 0);

begin

process (dw_fifo_empty_int, id_fifo_empty_int) is
begin
  for I in NUM_CHANNELS-1 downto 0 loop
      fsm_empty(I) <= id_fifo_empty_int(I) or dw_fifo_empty_int(I);
  end loop;
end process;

ipbus_fabric : entity work.ipbus_fabric_simple
  generic map(
    NSLV => NUM_CHANNELS,
    STROBE_GAP => false,
    DECODE_BASE => 8,
    DECODE_BITS => 8)
  port map (
    ipb_in => ipb_in,
    ipb_out => ipb_out,
    ipb_to_slaves => ipb_to_slaves,
    ipb_from_slaves => ipb_from_slaves
  );

gen_fifos : 
for n in NUM_CHANNELS-1 downto 0 generate

ipbus_input_fifos : entity work.emp_daqpath_ipbus_input_FIFOs
  generic map (n)
  port map(
    ipb_clk => ipb_clk,
    rx_clk => rx_clk,
    reset => rst,
    ipbus_in => ipb_to_slaves(n),
    ipbus_out => ipb_from_slaves(n),
      
    ts_fifo_re => ts_fifo_re(n),
    ts_fifo_rdata => ts_fifo_rdata(n),
    ts_fifo_empty => ts_fifo_empty_int(n),
    ts_fifo_full => ts_fifo_full(n),

    id_fifo_re => id_fifo_re(n),
    id_fifo_rdata => int_id_fifo_dout(n),
    id_fifo_empty => id_fifo_empty_int(n),
    id_fifo_full => id_fifo_full(n),

    nw_fifo_re => nw_fifo_re(n),
    nw_fifo_rdata => int_nw_fifo_dout(n),
    nw_fifo_empty => nw_fifo_empty_int(n),
    nw_fifo_full => nw_fifo_full(n),

    dw_fifo_re => word_fifo_re(n),
    dw_fifo_rdata => int_word_fifo_dout(n),
    dw_fifo_empty => dw_fifo_empty_int(n),
    dw_fifo_full => word_fifo_full(n)
  );
    
end generate gen_fifos;

gen_fsms : 
for n in NUM_CHANNELS-1 downto 0 generate
    
ipbus_fsms : entity work.emp_daqpath_ipbus_fsm  
  port map (
    clk => rx_clk,
    rst => rst,
    en => en,
    empty => fsm_empty(n),
    ts => ts_fifo_rdata(n),
    nw => int_nw_fifo_dout(n),
    ts_fifo_re => ts_fifo_re(n),
    id_fifo_re => id_fifo_re(n),
    id_fifo_we => int_id_fifo_we(n),
    nw_fifo_re => nw_fifo_re(n),
    nw_fifo_we => int_nw_fifo_we(n),
    data_fifo_re => word_fifo_re(n),
    data_fifo_we => int_word_fifo_we(n)
  );

end generate gen_fsms;

reg_nw : process(rx_clk, int_nw_fifo_dout)
begin
  if rising_edge(rx_clk) then
    for I in NUM_CHANNELS-1 downto 0 loop
      if (ts_fifo_re(I) = '1') then
        int_nw_fifo_dout_reg(I) <= int_nw_fifo_dout(I);
      end if;
    end loop;
  end if;
end process;

reg : process(rx_clk, int_word_fifo_we, int_id_fifo_we, int_nw_fifo_we, int_nw_fifo_dout_reg, int_id_fifo_dout, int_word_fifo_dout)
begin
  if rising_edge(rx_clk) then
    word_fifo_we <= int_word_fifo_we;
    id_fifo_we <= int_id_fifo_we;
    nw_fifo_we <= int_nw_fifo_we;
    word_fifo_dout <= int_word_fifo_dout;
    id_fifo_dout <= int_id_fifo_dout;
    nw_fifo_dout <= int_nw_fifo_dout_reg; 
    word_fifo_empty <= dw_fifo_empty_int;
    id_fifo_empty <= id_fifo_empty_int;
    nw_fifo_empty <= nw_fifo_empty_int;   
  end if;
end process;

end emp_daqpath_ipbus_input_FIFOs_array_Arch;
