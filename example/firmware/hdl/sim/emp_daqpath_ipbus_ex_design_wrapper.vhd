-- Wrapper entity for the N_CHAN input channels DAQPATH example design ipbus I/O simulation
-- Rev 26/04/2022

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.emp_daqpath_user_package.all;

entity emp_daqpath_ipbus_ex_design_wrapper is
    
  Port  (
    ipb_clk : in STD_LOGIC;
    rx_clk : in STD_LOGIC;
    dp_clk : in STD_LOGIC;
    tx_clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    ipb_in_addr : in STD_LOGIC_VECTOR(31 downto 0);
    ipb_in_wdata : in STD_LOGIC_VECTOR(31 downto 0);
    ipb_in_strobe : in STD_LOGIC;
    ipb_in_write : in STD_LOGIC;
    ipb_out_rdata : out STD_LOGIC_VECTOR(31 downto 0);
    ipb_out_ack : out STD_LOGIC;
    ipb_out_err : out STD_LOGIC;
    data_out : out STD_LOGIC_VECTOR(63 downto 0);
    data_we_out : out STD_LOGIC;
    idnw_out : out STD_LOGIC_VECTOR(31 downto 0);
    idnw_we_out : out STD_LOGIC;
    pause : in STD_LOGIC;
    slink_data : out STD_LOGIC_VECTOR(63 downto 0);
    slink_dv : out STD_LOGIC;
    slink_start : out STD_LOGIC;
    slink_end : out STD_LOGIC           
  );
             
end emp_daqpath_ipbus_ex_design_wrapper;

architecture emp_daqpath_ipbus_ex_design_wrapper_arch of emp_daqpath_ipbus_ex_design_wrapper is

signal    ipb_in : ipb_wbus;
signal    ipb_out : ipb_rbus;

constant  N_LINKS : integer := 1;
constant  N_MAIN_REGS : integer := 8;
constant  N_CH_REGS : integer := 4; 

begin

  ipb_in.ipb_addr <= ipb_in_addr;
  ipb_in.ipb_wdata <= ipb_in_wdata;
  ipb_in.ipb_strobe <= ipb_in_strobe;
  ipb_in.ipb_write <= ipb_in_write;
  ipb_out_rdata <= ipb_out.ipb_rdata;
  ipb_out_ack <= ipb_out.ipb_ack;
  ipb_out_err <= ipb_out.ipb_err;
 
  DAQPATH_ipbus_ex_design : entity work.emp_daqpath_ipbus_ex_design
	generic map (
	  NUM_CHANNELS => N_CHAN,
	  NUM_MAIN_REGS => N_MAIN_REGS,
	  NUM_CH_REGS => N_CH_REGS,
	  NUM_LINKS => N_LINKS
    )
    port map (
      ipb_clk => ipb_clk,
      rx_clk => rx_clk,
      dp_clk => dp_clk,
      tx_clk => tx_clk,
      rst => rst,
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      data_out => data_out,
      data_we_out => data_we_out,
      idnw_out => idnw_out,
      idnw_we_out => idnw_we_out,
      pause => pause, 
      slink_data => slink_data,
      slink_dv => slink_dv,
      slink_start => slink_start,
      slink_end => slink_end
    );
 
end emp_daqpath_ipbus_ex_design_wrapper_arch;
