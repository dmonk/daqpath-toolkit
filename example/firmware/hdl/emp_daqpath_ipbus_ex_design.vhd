-- Top entity for the DAQPATH ipbus example design. 
-- It uses the DAQPATH core to readout data from FIFOs of N_CHAN input channels.
-- Data is pre-loaded by ipbus scripts into a first level of FIFOs (ipbus input FIFOs), then transmitted to input FIFOs (derandomising buffers) according to a programmable timing scheme.
-- Data is read by daqpath core from N_CHAN channels and reorganised into a single output stream to feed ipbus readable output FIFOs.
-- Rev - 26/04/2022

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
library work;
use work.emp_daqpath_types_package.ALL;
use work.emp_daqpath_user_package.ALL;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_emp_daqpath_ex_design.all;

entity emp_daqpath_ipbus_ex_design is

  generic( 
    NUM_CHANNELS : integer :=16;
    NUM_MAIN_REGS : integer :=8;
    NUM_CH_REGS : integer :=4;
    NUM_LINKS : integer :=1
  );
  
  Port( 
    --clocks
    ipb_clk : in STD_LOGIC;
    rx_clk : in STD_LOGIC;
    dp_clk : in STD_LOGIC;
    tx_clk : in STD_LOGIC;
    --ipbus signals
    rst : in STD_LOGIC;
    ipb_in: in ipb_wbus;
    ipb_out: out ipb_rbus;
    --daqpath module outputs
    data_out : out STD_LOGIC_VECTOR(63 downto 0);
    data_we_out : out STD_LOGIC;
    idnw_out : out STD_LOGIC_VECTOR(31 downto 0);
    idnw_we_out : out STD_LOGIC;
    pause : in STD_LOGIC;
    slink_data : out STD_LOGIC_VECTOR(63 downto 0);
    slink_dv : out STD_LOGIC;
    slink_start : out STD_LOGIC;
    slink_end : out STD_LOGIC             
  );
             
end emp_daqpath_ipbus_ex_design;

architecture emp_daqpath_ipbus_ex_design_arch of emp_daqpath_ipbus_ex_design is

--------------------------------------------------------------
-- Signals declaration
-------------------------------------------------------------- 
--Ipbus signals
signal    ipbw : ipb_wbus_array(N_SLAVES-1 downto 0);
signal    ipbr : ipb_rbus_array(N_SLAVES-1 downto 0);
 
--Input fifos signals
signal    ipbus_id_fifo_wdata: ctrl_fifo_data_array(NUM_CHANNELS - 1 downto 0);
signal    id_ipbus_fifo_empty : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    id_ipbus_fifo_full : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    ipbus_id_fifo_we: STD_LOGIC_VECTOR(NUM_CHANNELS - 1 downto 0);

signal    ipbus_nw_fifo_wdata: ctrl_fifo_data_array(NUM_CHANNELS - 1 downto 0);
signal    nw_ipbus_fifo_empty : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    nw_ipbus_fifo_full : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    ipbus_nw_fifo_we: STD_LOGIC_VECTOR(NUM_CHANNELS - 1 downto 0);

signal    ipbus_word_fifo_wdata: data_fifo_data_array(NUM_CHANNELS - 1 downto 0);
signal    ipbus_word_fifo_we: STD_LOGIC_VECTOR(NUM_CHANNELS - 1 downto 0);
signal    word_ipbus_fifo_empty : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    word_ipbus_fifo_full : std_logic_vector(NUM_CHANNELS-1 downto 0);

signal    id_fifo_wdata : ctrl_fifo_data_array(NUM_CHANNELS-1 downto 0);
signal    id_fifo_rdata : ctrl_fifo_data_array(NUM_CHANNELS-1 downto 0);
signal    id_fifo_we : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    id_fifo_re : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    id_fifo_empty : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    id_fifo_full : std_logic_vector(NUM_CHANNELS-1 downto 0);

signal    nw_fifo_wdata : ctrl_fifo_data_array(NUM_CHANNELS-1 downto 0);
signal    nw_fifo_rdata : ctrl_fifo_data_array(NUM_CHANNELS-1 downto 0);
signal    nw_fifo_we : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    nw_fifo_re : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    nw_fifo_empty : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    nw_fifo_full : std_logic_vector(NUM_CHANNELS-1 downto 0);

signal    word_fifo_wdata : data_fifo_data_array(NUM_CHANNELS-1 downto 0);
signal    word_fifo_rdata_int : data_fifo_data_array(NUM_CHANNELS-1 downto 0);
signal    word_fifo_rdata : generic_data_fifo_data_array(NUM_CHANNELS-1 downto 0);
signal    word_fifo_we : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    word_fifo_re : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    word_fifo_empty : std_logic_vector(NUM_CHANNELS-1 downto 0);
signal    word_fifo_full : std_logic_vector(NUM_CHANNELS-1 downto 0);

--Output ipbus fifos signals
signal    output_data_fifo_wdata : data_fifo_data_array(0 to NUM_LINKS-1);
signal    output_data_fifo_we : std_logic_vector(0 to NUM_LINKS-1);
signal    output_data_fifo_empty : std_logic_vector(0 to NUM_LINKS-1);
signal    output_data_fifo_full : std_logic_vector(0 to NUM_LINKS-1);
signal    output_idnw_fifo_wdata : ipbus_data_array(0 to NUM_LINKS-1);
signal    output_idnw_fifo_we : std_logic_vector(0 to NUM_LINKS-1);
signal    output_idnw_fifo_empty : std_logic_vector(0 to NUM_LINKS-1);
signal    output_idnw_fifo_full : std_logic_vector(0 to NUM_LINKS-1);

--Main_csr and Channel_csr signals
signal    creg : ipb_reg_v(NUM_MAIN_REGS-1 downto 0) := (others => (others => '0'));
signal    sreg : ipb_reg_v(NUM_MAIN_REGS-1 downto 0) := (others => (others => '0'));
signal    ch_creg : ipbus_data_array2(NUM_CHANNELS-1 downto 0, NUM_CH_REGS-1 downto 0);
signal    ch_sreg : ipbus_data_array2(NUM_CHANNELS-1 downto 0, NUM_CH_REGS-1 downto 0);
signal    ch_ctrl0_reg : ipbus_data_array(NUM_CHANNELS - 1 downto 0);
signal    ch_ctrl1_reg : ipbus_data_array(NUM_CHANNELS - 1 downto 0);
signal    ch_ctrl2_reg : ipbus_data_array(NUM_CHANNELS - 1 downto 0);
signal    ch_ctrl3_reg : ipbus_data_array(NUM_CHANNELS - 1 downto 0);
signal    ch_stat0_reg : ipbus_data_array(NUM_CHANNELS - 1 downto 0);
signal    ch_stat1_reg : ipbus_data_array(NUM_CHANNELS - 1 downto 0);
signal    ch_evt_cnt_reg : ipbus_data_array(NUM_CHANNELS - 1 downto 0);
signal    ch_dw_cnt_reg : ipbus_data_array(NUM_CHANNELS - 1 downto 0);

--Enable/Reset signals
signal    trggen_en : STD_LOGIC;
signal    evtgen_en : STD_LOGIC;
signal    daqpath_en : STD_LOGIC;
signal    ipbus_fifo_en : STD_LOGIC;
signal    trggen_rst : STD_LOGIC;
signal    evtgen_rst : STD_LOGIC;
signal    daqpath_rst : STD_LOGIC;
signal    ipbus_fifo_rst : STD_LOGIC;
signal    int_rst : STD_LOGIC;

--Internal event generator signals
signal    evtgen_hdr_fifo_wdata: ipbus_data_array(NUM_CHANNELS - 1 downto 0);
signal    evtgen_dw_fifo_wdata: data_fifo_data_array(NUM_CHANNELS - 1 downto 0);
signal    evtgen_hdr_we: STD_LOGIC_VECTOR(NUM_CHANNELS - 1 downto 0);
signal    evtgen_dw_we: STD_LOGIC_VECTOR(NUM_CHANNELS - 1 downto 0);

--Other signals
signal    OUT_EV_CNT_OUT : std_logic_vector(31 downto 0);
signal    OUT_DW_CNT_OUT : std_logic_vector(31 downto 0);
signal    words_cnt : integer := 1;
signal    data_words : std_logic_vector(31 downto 0);
signal    dw_cnt : STD_LOGIC_VECTOR(31 downto 0);

--Daqpath core signals
signal    int_data_out : STD_LOGIC_VECTOR(63 downto 0);
signal    int_data_we_out : STD_LOGIC;
signal    int_idnw_out : STD_LOGIC_VECTOR(31 downto 0);
signal    int_idnw_we_out : STD_LOGIC;

begin

--------------------------------------------------------------
-- Main ipbus fabric
-------------------------------------------------------------- 
fabric: entity work.ipbus_fabric_sel
    generic map(
      NSLV => N_SLAVES,
      SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      sel => ipbus_sel_emp_daqpath_ex_design(ipb_in.ipb_addr),
      ipb_to_slaves => ipbw,
      ipb_from_slaves => ipbr
    );

--------------------------------------------------------------
-- MAIN status registers assignement
--------------------------------------------------------------
MAIN_sreg_proc : process (output_data_fifo_empty, output_idnw_fifo_empty, output_data_fifo_full, output_idnw_fifo_full, OUT_EV_CNT_OUT,OUT_DW_CNT_OUT)

begin

  for I in 0 to NUM_LINKS-1 loop
    sreg(0)(I*4) <= output_data_fifo_empty(I);
    sreg(0)(I*4+1) <= output_idnw_fifo_empty(I);
    sreg(0)(I*4+2) <= output_data_fifo_full(I);
    sreg(0)(I*4+3) <=  output_data_fifo_full(I);
  end loop;
  sreg(0)(31 downto 4*NUM_LINKS) <= (Others => '0');
  sreg(1) <= x"E1000000";
  sreg(2) <= OUT_EV_CNT_OUT;
  sreg(3) <= OUT_DW_CNT_OUT;
  sreg(4) <= x"E4001005";
  sreg(5) <= x"E5002021";
end process;

process(dp_clk,int_idnw_out,int_idnw_we_out)	
	begin
		if rising_edge(dp_clk) then
			if rst = '1' then
				sreg(6)(15 downto 0) <= (others => '0');
				words_cnt <= 0;
			elsif int_idnw_we_out = '1' then
				sreg(6)(15 downto 0) <= int_idnw_out(31 downto 16);
				words_cnt <= words_cnt + to_integer(unsigned(int_idnw_out(15 downto 0)));
			end if;
		end if;
 end process;

sreg(6)(31 downto 16) <= (others => '0');
data_words <= std_logic_vector(to_unsigned(words_cnt, data_words'length));
sreg(7)(31 downto 0) <= std_logic_vector(to_unsigned(words_cnt, data_words'length)); 

--------------------------------------------------------------
-- MAIN status registers syncreg entity
-------------------------------------------------------------- 
IPBUS_MAIN_REGS : entity work.ipbus_syncreg_v
  generic map(
    N_CTRL => NUM_MAIN_REGS,
    N_STAT => NUM_MAIN_REGS
  )
  port map(
    clk        => ipb_clk,
    rst        => rst,    
    ipb_in     => ipbw(N_SLV_MAIN_CSR),
    ipb_out    => ipbr(N_SLV_MAIN_CSR),
    slv_clk    => dp_clk,
    d          => sreg,
    q          => creg
  );  

--------------------------------------------------------------
-- ctrl signals retrival from main CSRs
--------------------------------------------------------------
trggen_en <= creg(0)(0);
trggen_rst <= (not creg(0)(15)) or creg(0)(1);
evtgen_en <= creg(0)(2);
evtgen_rst <= (not creg(0)(15)) or creg(0)(3);
daqpath_en <= creg(0)(4);
daqpath_rst <= (not creg(0)(15)) or creg(0)(5);
ipbus_fifo_en <= creg(0)(6);
ipbus_fifo_rst <= (not creg(0)(15)) or creg(0)(7);
int_rst <= rst or not creg(0)(15);

--------------------------------------------------------------
-- IPBUS input FIFOs block
-------------------------------------------------------------- 
IPBUS_Input_FIFOs : entity work.emp_daqpath_ipbus_input_FIFOs_array
generic map (NUM_CHANNELS => NUM_CHANNELS)
port map(  
  ipb_clk => ipb_clk,
  rx_clk => rx_clk,
  rst => int_rst,
  en => ipbus_fifo_en,
  ipb_in => ipbw(N_SLV_IN),
  ipb_out => ipbr(N_SLV_IN),
  id_fifo_dout => ipbus_id_fifo_wdata,
  nw_fifo_dout => ipbus_nw_fifo_wdata,
  word_fifo_dout => ipbus_word_fifo_wdata,
  id_fifo_we => ipbus_id_fifo_we,
  nw_fifo_we => ipbus_nw_fifo_we,
  word_fifo_we => ipbus_word_fifo_we,
  id_fifo_empty => id_ipbus_fifo_empty,
  nw_fifo_empty => nw_ipbus_fifo_empty,
  word_fifo_empty => word_ipbus_fifo_empty,
  id_fifo_full => id_ipbus_fifo_full,
  nw_fifo_full => nw_ipbus_fifo_full,
  word_fifo_full => word_ipbus_fifo_full
);
 
--------------------------------------------------------------
-- Channel CSRegs assignements 
--------------------------------------------------------------
ch_stat0_proc : process (ipbus_id_fifo_wdata,word_ipbus_fifo_empty, word_ipbus_fifo_full, id_ipbus_fifo_full, id_ipbus_fifo_empty)
begin
  for I in NUM_CHANNELS-1 downto 0 loop
    ch_stat0_reg(I)(0) <= word_ipbus_fifo_empty(I);
    ch_stat0_reg(I)(1) <= id_ipbus_fifo_empty(I);
    ch_stat0_reg(I)(2) <= word_ipbus_fifo_full(I);
    ch_stat0_reg(I)(3) <= id_ipbus_fifo_full(I);
    ch_stat0_reg(I)(7 downto 4) <= (others => '0');
    ch_stat0_reg(I)(23 downto 8) <= ipbus_id_fifo_wdata(I);
     ch_stat0_reg(I)(31 downto 24) <= std_logic_vector(to_unsigned(I,8));
  end loop;  
end process;

ch_stat1_proc : process (id_fifo_rdata,word_fifo_empty, word_fifo_full, id_fifo_full, id_fifo_empty)
begin
  for I in NUM_CHANNELS-1 downto 0 loop
    ch_stat1_reg(I)(0) <= word_fifo_empty(I);
    ch_stat1_reg(I)(1) <= id_fifo_empty(I);
    ch_stat1_reg(I)(2) <= word_fifo_full(I);
    ch_stat1_reg(I)(3) <= id_fifo_full(I);
    ch_stat1_reg(I)(7 downto 4) <= (others => '0');
    ch_stat1_reg(I)(23 downto 8) <= id_fifo_rdata(I);
    ch_stat1_reg(I)(31 downto 24) <= std_logic_vector(to_unsigned(I,8));
  end loop;  
end process;

process (ch_creg,ch_stat0_reg,ch_stat1_reg,ch_evt_cnt_reg,ch_dw_cnt_reg)
begin
  for I in NUM_CHANNELS-1 downto 0 loop
    ch_ctrl0_reg(I) <= ch_creg(I,0);
    ch_ctrl1_reg(I) <= ch_creg(I,1);
    ch_ctrl2_reg(I) <= ch_creg(I,2);
    ch_ctrl3_reg(I) <= ch_creg(I,3);
    ch_sreg(I,0) <= ch_stat0_reg(I);
    ch_sreg(I,1) <= ch_stat1_reg(I);
    ch_sreg(I,2) <= ch_evt_cnt_reg(I);
    ch_sreg(I,3) <= ch_dw_cnt_reg(I);
  end loop;
end process;

--------------------------------------------------------------
-- Channel CSRegs block
-------------------------------------------------------------- 
IPBUS_CH_REGISTERs : entity work.emp_daqpath_chan_ipbus_csregs
  generic map(
    NUM_CHANNELS => NUM_CHANNELS,
    NUM_REGISTERS => NUM_CH_REGS
  )
  port map(
    ipb_clk => ipb_clk,
    dp_clk => dp_clk,
    reset => rst,
    ipbus_in => ipbw(N_SLV_CH_CSR),
    ipbus_out => ipbr(N_SLV_CH_CSR),
    creg => ch_creg,
    sreg => ch_sreg
  );

--------------------------------------------------------------
-- Internal event generator block 
-------------------------------------------------------------- 
EVTGEN : entity work.emp_daqpath_evtgen_array
  generic map (NUM_CHANNELS => NUM_CHANNELS)
  port map(
    clk => rx_clk,
	rst => int_rst,
	trggen_en => trggen_en,
	evtgen_en => evtgen_en,
	ch_ctrl0_reg => ch_ctrl0_reg,
	ch_ctrl1_reg => ch_ctrl1_reg,
	ch_ctrl2_reg => ch_ctrl2_reg,
	ch_ctrl3_reg => ch_ctrl3_reg,
	ch_evt_cnt_reg => ch_evt_cnt_reg,
	ch_dw_cnt_reg => ch_dw_cnt_reg,
	hdr_fifo_wdata => evtgen_hdr_fifo_wdata,
	dw_fifo_wdata => evtgen_dw_fifo_wdata,
	hdr_we => evtgen_hdr_we,
	dw_we => evtgen_dw_we
  );

--------------------------------------------------------------
-- Process selecting data from IPBUS or Internal event generator
--------------------------------------------------------------
fifo_wdata_proc : process (evtgen_en,evtgen_hdr_fifo_wdata,evtgen_hdr_we, evtgen_dw_fifo_wdata,evtgen_dw_we, ipbus_id_fifo_wdata, ipbus_id_fifo_we, ipbus_nw_fifo_wdata,ipbus_nw_fifo_we, ipbus_word_fifo_wdata, ipbus_word_fifo_we)
begin
  for I in NUM_CHANNELS-1 downto 0 loop
    if (evtgen_en = '1') then
      id_fifo_wdata(I) <=  evtgen_hdr_fifo_wdata(I)(31 downto 16);
      id_fifo_we(I) <= evtgen_hdr_we(I);
      nw_fifo_wdata(I) <=  evtgen_hdr_fifo_wdata(I)(15 downto 0);
      nw_fifo_we(I) <= evtgen_hdr_we(I);
      word_fifo_wdata(I) <= evtgen_dw_fifo_wdata(I);
      word_fifo_we(I) <= evtgen_dw_we(I);
    else
      id_fifo_wdata(I) <=  ipbus_id_fifo_wdata(I);
      id_fifo_we(I) <= ipbus_id_fifo_we(I);
      nw_fifo_wdata(I) <=  ipbus_nw_fifo_wdata(I);
      nw_fifo_we(I) <= ipbus_nw_fifo_we(I);
      word_fifo_wdata(I) <= ipbus_word_fifo_wdata(I);
      word_fifo_we(I) <= ipbus_word_fifo_we(I);
    end if;
  end loop;  
end process;

input_wdata_proc: process(word_fifo_rdata_int)
begin
  for I in 0 to NUM_CHANNELS-1 loop
    word_fifo_rdata(I) <= word_fifo_rdata_int(I)(8*DW_BYTES-1 downto 0);
  end loop;
end process;

--------------------------------------------------------------
-- Input FIFOs block
--------------------------------------------------------------
Input_FIFOs : entity work.emp_daqpath_input_FIFOs_array
generic map (NUM_CHANNELS => NUM_CHANNELS)
port map(  
  rx_clk => rx_clk,
  dp_clk => dp_clk,
  rst => int_rst,
  id_fifo_din  => id_fifo_wdata,
  nw_fifo_din  => nw_fifo_wdata,
  word_fifo_din  => word_fifo_wdata,
  id_fifo_dout => id_fifo_rdata,
  nw_fifo_dout  => nw_fifo_rdata,
  word_fifo_dout  => word_fifo_rdata_int,
  id_fifo_we  => id_fifo_we,
  nw_fifo_we  => nw_fifo_we,
  word_fifo_we  => word_fifo_we,
  id_fifo_re  => id_fifo_re,
  nw_fifo_re  => nw_fifo_re,
  word_fifo_re  => word_fifo_re,
  id_fifo_empty => id_fifo_empty,
  nw_fifo_empty => nw_fifo_empty,
  word_fifo_empty => word_fifo_empty,
  id_fifo_full => id_fifo_full,
  nw_fifo_full => nw_fifo_full,
  word_fifo_full => word_fifo_full
);

--------------------------------------------------------------
-- Daqpath core block
--------------------------------------------------------------
DAQPATH_MODULE : entity work.emp_daqpath_module
generic map (
  NUM_CHANNELS => NUM_CHANNELS,
  N_CHAN_PER_GROUP => N_CHAN_PER_GROUP
)
port map(
  --ipbus
  ipb_clk => ipb_clk,
  ipb_rst => rst,
  ipb_in => ipbw(N_SLV_DAQPATH_CSR),
  ipb_out => ipbr(N_SLV_DAQPATH_CSR),  
  clk => dp_clk,
  rst => int_rst,
  en => daqpath_en,
  pause => pause,
  data_out => int_data_out,
  data_we_out => int_data_we_out,
  idnw_out => int_idnw_out,
  idnw_we_out => int_idnw_we_out,
  slink_data => slink_data,
  slink_dv => slink_dv,
  slink_start => slink_start,
  slink_end => slink_end,
  word_fifo_rdata  => word_fifo_rdata,
  nw_fifo_rdata  => nw_fifo_rdata,
  id_fifo_rdata  => id_fifo_rdata,
  id_fifo_re  => id_fifo_re,
  nw_fifo_re  => nw_fifo_re,
  word_fifo_re  => word_fifo_re,
  word_fifo_empty => word_fifo_empty,
  id_fifo_empty => id_fifo_empty,
  nw_fifo_empty => nw_fifo_empty
);

--------------------------------------------------------------
-- Output FIFOs data selection
--------------------------------------------------------------
process (int_data_out, int_data_we_out, int_idnw_out, int_idnw_we_out)
  begin
    for n in 0 to NUM_LINKS-1 loop
      output_data_fifo_wdata(n) <= int_data_out;
      output_data_fifo_we(n) <= int_data_we_out;
      output_idnw_fifo_wdata(n) <= int_idnw_out;
      output_idnw_fifo_we(n) <= int_idnw_we_out;
  end loop;  
end process;

--------------------------------------------------------------
-- Output FIFOs block
--------------------------------------------------------------
IPBUS_Output_FIFOs : entity work.emp_daqpath_ipbus_output_FIFOs_array
generic map (NUM_CHANNELS => NUM_LINKS)
port map ( 
  dp_clk => dp_clk,
  ipb_clk => ipb_clk,
  rst => int_rst,
  en => daqpath_en,
  ipb_in => ipbw(N_SLV_OUT),
  ipb_out => ipbr(N_SLV_OUT),
  data_fifo_wdata => output_data_fifo_wdata,
  data_fifo_we => output_data_fifo_we,
  data_fifo_empty => output_data_fifo_empty,
  data_fifo_full => output_data_fifo_full,
  idnw_fifo_wdata => output_idnw_fifo_wdata,
  idnw_fifo_we => output_idnw_fifo_we,
  idnw_fifo_empty => output_idnw_fifo_empty,
  idnw_fifo_full => output_idnw_fifo_full    
);

--------------------------------------------------------------
-- Debug event/words counters
--------------------------------------------------------------
-- Counter that counts total transmitted events	
OUT_EV_CNT : entity work.emp_daqpath_clk_cnt
  port map ( 
    clk => dp_clk,
    rst => int_rst,
    en => int_idnw_we_out,
    cnt_out => OUT_EV_CNT_OUT
  );

-- Counter that counts total transmitted words  
OUT_DW_CNT : entity work.emp_daqpath_clk_cnt
  port map ( 
    clk => dp_clk,
    rst => int_rst,
    en => int_data_we_out,
    cnt_out => OUT_DW_CNT_OUT
  );

--------------------------------------------------------------
-- Output signals for higher hierarchy level
--------------------------------------------------------------
data_out <= int_data_out;
data_we_out <= int_data_we_out;
idnw_out <= int_idnw_out;
idnw_we_out <= int_idnw_we_out;
  
end emp_daqpath_ipbus_ex_design_arch;