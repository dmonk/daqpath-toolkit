-- DAQPATH core block (Pipelined version) - Containing channel blocks, interfaces with input fifos and provides the output stream
-- Rev - 07/07/2022

-- Version 2.0 - Added :
--    1)  Data formatter module to provide 64bit output stream with programmable input words size of 1,2,4,8 DW_BYTES
--    2)  Internal ipbus csregs
--    3)  Channel mask for unused channels (ipbus set)
--    4)  Programmable header option (ipbus set)
--    5)  Programmable Slink FIFO words depth


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.emp_daqpath_types_package.ALL;
use work.emp_daqpath_user_package.ALL;
use work.ipbus.all;
use work.ipbus_reg_types.all;

entity emp_daqpath_module is

  generic( 
    NUM_CHANNELS : integer :=16;
    N_CHAN_PER_GROUP : integer := 4;
    Slink_fifo_data_width : integer := 32768
  );    
  Port (
    --ipbus ports
    ipb_clk : in STD_LOGIC;    
    ipb_in: in ipb_wbus;
    ipb_out: out ipb_rbus;    
    ipb_rst : in STD_LOGIC;
    -- Input ports
    clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    en : in STD_LOGIC;
    pause : in std_logic;
    word_fifo_empty : IN STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    id_fifo_empty : IN STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    nw_fifo_empty : IN STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    word_fifo_rdata : in generic_data_fifo_data_array(NUM_CHANNELS-1 downto 0);
    id_fifo_rdata : in ctrl_fifo_data_array(NUM_CHANNELS-1 downto 0);
    nw_fifo_rdata : in ctrl_fifo_data_array(NUM_CHANNELS-1 downto 0);
    -- Output ports
    data_out : out STD_LOGIC_VECTOR(63 downto 0);
    data_we_out : out STD_LOGIC;
    idnw_out : out STD_LOGIC_VECTOR(31 downto 0);
    idnw_we_out : out STD_LOGIC;
    slink_data: out std_logic_vector(63 downto 0);
    slink_dv: out std_logic;
    slink_start: out std_logic;
    slink_end: out std_logic;
    word_fifo_re : OUT STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    id_fifo_re : OUT STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    nw_fifo_re : OUT STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0)    
  );

end emp_daqpath_module;

architecture emp_daqpath_module_arch of emp_daqpath_module is

constant  N_PIPE_GROUPS : integer := (NUM_CHANNELS + N_CHAN_PER_GROUP - 1) / N_CHAN_PER_GROUP;

--Channel chain signals
type ch_nbit_array is array (NUM_CHANNELS-1 downto 1) of STD_LOGIC_VECTOR (8*DW_BYTES -1 downto 0);
type ch_1bit_array is array (NUM_CHANNELS-1 downto 1) of STD_LOGIC;

signal ch_token_in, ch_we_out : ch_1bit_array;
signal ch_data_out : ch_nbit_array;

--Main FSM signals
signal current_id_int : STD_LOGIC_VECTOR(15 downto 0);
signal fsm_token_out, fsm_token_in : STD_LOGIC;
signal daqpath_empty_signal : STD_LOGIC_VECTOR(NUM_CHANNELS - 1 downto 0);

--Data words counter signals
signal words_cnt : integer := 1;
signal data_words : std_logic_vector(15 downto 0);

--Slink_FIFOs and output signals
signal int_data_out : STD_LOGIC_VECTOR(63 downto 0);
signal int_data_we_out, data_we_out_nbit : STD_LOGIC;
signal data_out_nbit : STD_LOGIC_VECTOR (8*DW_BYTES -1 downto 0);
signal int_idnw_out : STD_LOGIC_VECTOR(31 downto 0);
signal int_idnw_we_out : STD_LOGIC;
signal slink_data_fifo_empty : std_logic;
signal slink_data_fifo_full : std_logic;
signal slink_idnw_fifo_empty : std_logic;
signal slink_idnw_fifo_full : std_logic;

-- pipeline for ctrls
type pipeline_data is array (0 to N_PIPE_GROUPS) of STD_LOGIC_VECTOR(17 downto 0);
signal reg_data : pipeline_data;

-- ipbus stuff
signal hdr_en, hdr_en_reg : std_logic;
signal channel_mask, channel_mask_ch, channel_mask_main  : std_logic_vector(NUM_CHANNELS-1 downto 0) := (Others => '0');
signal daqpath_creg            : ipb_reg_v(3 downto 0) := (others => (others => '0'));
signal daqpath_sreg            : ipb_reg_v(3 downto 0) := (others => (others => '0'));

begin

--------------------------------------------------------------
-- Daqpath ipbus ctrl/status registers
-------------------------------------------------------------- 
IPBUS_DAQPATH_REGS : entity work.ipbus_syncreg_v
  generic map(
    N_CTRL => 4,
    N_STAT => 4
  )
  port map(
    clk        => ipb_clk,
    rst        => ipb_rst,    
    ipb_in     => ipb_in,
    ipb_out    => ipb_out,
    slv_clk    => clk,
    d          => daqpath_sreg,
    q          => daqpath_creg
  );

-- Set bit 0 in daqpath_ctrl_reg0 will disable header generation
-- if hdr_en    -> at least 1/2 header words of DW_BYTES is/are inserted in each channel packet
--              if DW_BYTES >2 -> 1 header word of DW_BYTES bytes ("ff...." + "channelx_id"(8 bits) + "number_of_input_words_in_channelx_packet"(8 bits))
--              if DW_BYTES =2 -> 1 header word of 2 bytes ("channelx_id"(8 bits) + "number_of_input_words_in_channelx_packet"(8 bits))
--              if DW_BYTES =2 -> 2 header words of 1 byte ("channelx_id"(8 bits) ) and ("number_of_input_words_in_channelx_packet"(8 bits))
hdr_en <= daqpath_creg(0)(0);
daqpath_sreg(2)(31 downto 24) <= x"E2";
daqpath_sreg(3)(31 downto 24) <= x"E3";

channel_mask_reg_proc : process(clk, channel_mask)
begin
  if rising_edge(clk) then
    channel_mask_ch <= channel_mask;
    channel_mask_main <= channel_mask;
  end if;
end process;

--===========================================================--
--  Channel mask (from ipbus regs so far..)
--===========================================================--

---- Assuming N_CHANNELS <= 32
--channel_mask(NUM_CHANNELS-1 downto 0) <= daqpath_creg(1)(NUM_CHANNELS-1 downto 0);
--daqpath_sreg(0)(31 downto 24) <= x"E0";
--daqpath_sreg(1)(31 downto 24) <= x"E1";
--daqpath_sreg(0)(NUM_CHANNELS-1 downto 0) <= channel_mask(NUM_CHANNELS-1 downto 0); 
--daqpath_sreg(1)(NUM_CHANNELS-1 downto 0) <= daqpath_empty_signal;

--  Version for generic num_channels (max 36 in applications?)
--  Channel mask is a NUM_CHANNELS bit register, assigned by ipbus in this moment (splitted into two ipbus regs for NUM_CHANNELS > 32)
ch_mask_proc: process (daqpath_creg)
begin
  if(NUM_CHANNELS <= 32) then
    channel_mask(NUM_CHANNELS-1 downto 0) <= daqpath_creg(1)(NUM_CHANNELS-1 downto 0);
  else
    channel_mask(31 downto 0) <= daqpath_creg(1);
    channel_mask(NUM_CHANNELS-1 downto 32) <= daqpath_creg(2)(NUM_CHANNELS-32-1 downto 0);
  end if;
end process;

daqpath_sreg_proc: process (channel_mask)
begin
  if(NUM_CHANNELS <= 32) then
    daqpath_sreg(0)(NUM_CHANNELS-1 downto 0) <= channel_mask(NUM_CHANNELS-1 downto 0);
  else
    daqpath_sreg(0) <= channel_mask(31 downto 0);
    daqpath_sreg(1)(NUM_CHANNELS-32-1 downto 0) <= channel_mask(NUM_CHANNELS-1 downto 32);
  end if;
end process;

--------------------------------------------------------------
--  Daqpath trigger signal based on empty signals from input fifos
--  Event readout starts when input fifos for all channels contain at least 1 event to read 
--------------------------------------------------------------
process (word_fifo_empty, id_fifo_empty, nw_fifo_empty) is
begin
  for I in NUM_CHANNELS-1 downto 0 loop
      daqpath_empty_signal(I) <= id_fifo_empty(I) or word_fifo_empty(I) or nw_fifo_empty(I);
  end loop;
end process;

-- output
data_we_out <= int_data_we_out;
data_out <= int_data_out;
idnw_we_out <= int_idnw_we_out;
idnw_out <= int_idnw_out;

--pipeline chain for control signals
reg_data(0)(0) <= fsm_token_in;
reg_data(0)(1) <= fsm_token_out;
reg_data(0)(17 downto 2) <= current_id_int;

gen_pipeline : 
for n in 1 to N_PIPE_GROUPS generate

  Stagex : entity work.emp_daqpath_pipeline_stage  
    generic map (
      NBIT => 18
    )
    port map (
      clk => clk,
      rst => rst,
      din => reg_data(n-1),
      dout => reg_data(n)
    );
end generate gen_pipeline;

-----------------------------------------------------------
--  emp_daqpath_channels (pipelined in groups)
-----------------------------------------------------------

gen_channels_loop1 : for i in 0 to N_PIPE_GROUPS-1 generate
  gen_channels_loop2 : for j in 0 to N_CHAN_PER_GROUP-1 generate
  
  constant n : integer := i*N_CHAN_PER_GROUP + j;
  
  begin
  
  --NULL instance for n >= N_CHAN
  Ch_null : if n>=NUM_CHANNELS generate
    process(clk)
      begin
        null;
      end process;
  end generate Ch_null;
  
  -- First channel of the group
  Ch0 : if j=0 and n<NUM_CHANNELS generate
    
    -- Channel 0 (First channel of first group)
    Ch0_0 : if i=0 generate
        channel0_0 : entity work.emp_daqpath_channel 
          generic map (n, DW_BYTES, N_PIPE_GROUPS - 1 - i, true)
          port map (
            clk => clk,
            rst => rst,
            en => en,
            hdr_en => hdr_en,
            mask => channel_mask_ch(n),
            token_in => fsm_token_out,
            current_id => current_id_int,
            we_in => ch_we_out(1),
            data_in => ch_data_out(1),
            token_out => ch_token_in(1),
            data_out => data_out_nbit,
            we_out => data_we_out_nbit,
            id_fifo_re => id_fifo_re(0),
            nw_fifo_re => nw_fifo_re(0),
            data_fifo_re => word_fifo_re(0),
            nw_fifo_rdata => nw_fifo_rdata(0),
            id_fifo_rdata => id_fifo_rdata(0),
            word_fifo_rdata => word_fifo_rdata(0)
          );
      end generate Ch0_0;
  
  -- First channel of i-th group
    Ch0_x : if i>0 and n<NUM_CHANNELS-1 generate
        channel0_x : entity work.emp_daqpath_channel 
          generic map (n, DW_BYTES, N_PIPE_GROUPS - 1 - i , true)
          port map (
            clk => clk,
            rst => rst,
            en => en,
            hdr_en => hdr_en,
            mask => channel_mask_ch(n),
            token_in => ch_token_in( n ) ,
            current_id => current_id_int,
            we_in => ch_we_out( n+1 ),
            data_in => ch_data_out( n+1 ),
            token_out => ch_token_in( n+1),
            data_out => ch_data_out( n ),
            we_out => ch_we_out( n ),
            id_fifo_re => id_fifo_re( n ),
            nw_fifo_re => nw_fifo_re( n ),
            data_fifo_re => word_fifo_re( n ),
            nw_fifo_rdata => nw_fifo_rdata( n ),
            id_fifo_rdata => id_fifo_rdata( n ),
            word_fifo_rdata => word_fifo_rdata( n )
          );
      end generate Ch0_x;
  
  -- First channel of i-th group in case it's also the last one
  -- As ex : you have 5 channels and 2 pipe groups of four channels) 
      Ch0_x_last : if i>0 and n=NUM_CHANNELS-1 generate
        channel0_x_last : entity work.emp_daqpath_channel  
          generic map (n, DW_BYTES, N_PIPE_GROUPS - 1 - i , true)
          port map (
            clk => clk,
            rst => rst,
            en => en,
            hdr_en => hdr_en,
            mask => channel_mask_ch(n),
            token_in => ch_token_in( n ) ,
            current_id => current_id_int,
            we_in => '0',
            data_in => (others => '0'),
            token_out => fsm_token_in,
            data_out => ch_data_out( n ),
            we_out => ch_we_out( n ),
            id_fifo_re => id_fifo_re( n ),
            nw_fifo_re => nw_fifo_re( n ),
            data_fifo_re => word_fifo_re( n ),
            nw_fifo_rdata => nw_fifo_rdata( n ),
            id_fifo_rdata => id_fifo_rdata( n ),
            word_fifo_rdata => word_fifo_rdata( n )
          );
      end generate Ch0_x_last;
        
  end generate Ch0;
  
  -- Other channels (Not the first of the group - so not registered)
  Chx : if j/=0 and n<NUM_CHANNELS generate
     
      Chx_x : if n /= NUM_CHANNELS - 1 generate
        channelx_x : entity work.emp_daqpath_channel 
          generic map (n, DW_BYTES, N_PIPE_GROUPS - 1 - i, false)
          port map (
            clk => clk,
            rst => rst,
            en => en,
            hdr_en => hdr_en,
            mask => channel_mask_ch(n),
            token_in => ch_token_in(n) ,
            current_id => current_id_int,
            we_in => ch_we_out(n+1),
            data_in => ch_data_out(n+1),
            token_out => ch_token_in(n+1),
            data_out => ch_data_out(n),
            we_out => ch_we_out(n),
            id_fifo_re => id_fifo_re(n),
            nw_fifo_re => nw_fifo_re(n),
            data_fifo_re => word_fifo_re(n),
            nw_fifo_rdata => nw_fifo_rdata(n),
            id_fifo_rdata => id_fifo_rdata(n),
            word_fifo_rdata => word_fifo_rdata(n)
          );
        end generate Chx_x;
        
        Chx_last : if n= NUM_CHANNELS - 1 generate
        channelx_last : entity work.emp_daqpath_channel  
          generic map (n, DW_BYTES, N_PIPE_GROUPS - 1 - i, false)
          port map (
            clk => clk,
            rst => rst,
            en => en,
            hdr_en => hdr_en,
            mask => channel_mask_ch(n),
            token_in => ch_token_in(n) ,
            current_id => current_id_int,
            we_in => '0',
            data_in => (others => '0'),
            token_out => fsm_token_in,
            data_out => ch_data_out(n),
            we_out => ch_we_out(n),
            id_fifo_re => id_fifo_re(n),
            nw_fifo_re => nw_fifo_re(n),
            data_fifo_re => word_fifo_re(n),
            nw_fifo_rdata => nw_fifo_rdata(n),
            id_fifo_rdata => id_fifo_rdata(n),
            word_fifo_rdata => word_fifo_rdata(n)
          );
        end generate Chx_last;
     
      end generate Chx;    
    
  end generate gen_channels_loop2;
end generate gen_channels_loop1;

--------------------------------------------------------------
-- Main FSM block
--------------------------------------------------------------
Main_logic_FSM : entity work.emp_daqpath_main_fsm
    generic map(NUM_CHANNELS)
    port map(
        clk => clk,
        en => en,
        rst => rst,
        token_in => fsm_token_in,
        token_out => fsm_token_out,
        current_id => current_id_int,
        ch_mask => channel_mask_main,
        empty => daqpath_empty_signal
    );    

--------------------------------------------------------------
-- Slink FIFOs block
--------------------------------------------------------------
SLINK_Output_FIFOs : entity work.emp_daqpath_slink_FIFOs
  generic map (Data_fifo_word_depth => Slink_fifo_data_width)
  port map (
  	clk => clk,
    en => en,
    reset => rst,
  	idnw_fifo_wdata => int_idnw_out,
  	idnw_fifo_we =>  int_idnw_we_out,
  	idnw_fifo_empty => slink_idnw_fifo_empty,
  	idnw_fifo_full => slink_idnw_fifo_full,
  	dw_fifo_wdata => int_data_out,
  	dw_fifo_we => int_data_we_out,
  	dw_fifo_empty => slink_data_fifo_empty,
  	dw_fifo_full => slink_data_fifo_full,
    pause => pause,
  	slink_data => slink_data,
  	slink_dv => slink_dv,
  	slink_start => slink_start,
  	slink_end => slink_end
  );

--------------------------------------------------------------
-- Data Formatter block
--------------------------------------------------------------
Data_Formatter : entity work.emp_daqpath_data_formatter
  generic map (
    DW_BYTES => DW_BYTES
  )
  port map(  
    clk => clk,
    rst => rst,
    ev_id => reg_data(N_PIPE_GROUPS)(17 downto 2),
    start => reg_data(N_PIPE_GROUPS)(1),
    last => reg_data(N_PIPE_GROUPS)(0),
    data_out => int_data_out,
    data_we_out => int_data_we_out,
    idnw_out => int_idnw_out,
    idnw_we_out => int_idnw_we_out,
    data_in => data_out_nbit,
    data_we_in => data_we_out_nbit
  );

end emp_daqpath_module_arch;