-- FSM for internal events transmission to input fifos 
-- Rev. 26/09/2022

library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity emp_daqpath_evtgen_fsm is
port (
    clk : IN STD_LOGIC;
    en : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    trg : IN STD_LOGIC;
    nw : IN STD_LOGIC_VECTOR(7 downto 0);
    ne : IN STD_LOGIC_VECTOR(15 downto 0);
    header : OUT STD_LOGIC_VECTOR(31 downto 0);
    hdr_sel : OUT STD_LOGIC;
    prbs_en : OUT STD_LOGIC;
    dw_sel : OUT STD_LOGIC   
);
end emp_daqpath_evtgen_fsm;


ARCHITECTURE emp_daqpath_evtgen_fsm_arch of emp_daqpath_evtgen_fsm is


type state_type is (S_IDLE, S_HEADER, S_DW, S_LAST_DW, S_END);
signal cs: state_type;
signal ns: state_type;

signal nw_int : integer;
signal nw_cnt : integer;
signal ne_int : integer;
signal ne_cnt : integer;
signal cur_id_cnt : integer := 0;

signal current_id : std_logic_vector(15 downto 0);

begin



cnt_proc : process (cur_id_cnt,cs,clk,rst)
begin
    if (rst = '1') then
      cur_id_cnt <= 1;
    elsif rising_edge(clk) then
      if (cs = S_HEADER) then
        cur_id_cnt <= cur_id_cnt + 1;
      else
        cur_id_cnt <= cur_id_cnt;
      end if;
    end if;
end process;

current_id <= std_logic_vector(to_unsigned(cur_id_cnt, 16));

ne_cnt_proc : process (cs,clk,rst)
begin
    if (rst = '1') then
      ne_cnt <= 1;
    elsif rising_edge(clk) then
      if (cs = S_LAST_DW) then
        ne_cnt <= ne_cnt + 1;
      end if;
    end if;
end process;

nw_cnt_proc : process (cs,clk,rst)
begin
    if (rst = '1') then
      nw_cnt <= 1;
    elsif rising_edge(clk) then
      if (cs = S_DW) then
        nw_cnt <= nw_cnt + 1;
      elsif (cs = S_LAST_DW) then
        nw_cnt <= 1;
      end if;
    end if;
end process;

header_regs: process (current_id,nw,ne,trg,rst,clk)
  begin
    if (rst = '1') then
       header(31 downto 0) <= (others => '0');
       nw_int <= 0;
       ne_int <= 0;
    elsif rising_edge(clk) and (trg = '1') then
       header(31 downto 16) <= current_id;
       header(15 downto 8) <= "00000000";
       header(7 downto 0) <= nw;
       nw_int <= TO_INTEGER(unsigned(nw));
       ne_int <= TO_INTEGER(unsigned(ne));
    end if;
  end process;

FSM_regs: process (ns,rst,en,clk)
  begin
    if (rst = '1') then
      cs <= S_IDLE;
    elsif rising_edge(clk) and (en = '1') then
      cs <= ns;
    end if;
  end process;

FSM_logic: process (nw_cnt,nw_int,ne_cnt,ne_int,trg,en,cs)
  begin
    case cs is
    
      when S_IDLE =>  

        hdr_sel <= '0';
        prbs_en <= '0';
        dw_sel <= '0';
        if (trg = '1') and (en = '1') then
            ns <= S_HEADER;
        else
            ns <= S_IDLE;
        end if;  

      when S_HEADER =>

        hdr_sel <= '1';
        prbs_en <= '1';
        dw_sel <= '0';
        ns <= S_DW;
    
      when S_DW =>

        hdr_sel <= '0';
        prbs_en <= '1';
        dw_sel <= '1';
        if (nw_cnt = nw_int - 1) then
            ns <= S_LAST_DW;
        else
            ns <= S_DW;
        end if;
     
      when S_LAST_DW =>

        hdr_sel <= '0';
        prbs_en <= '0';
        dw_sel <= '1';
        if (ne_cnt = ne_int) then
            ns <= S_END;
        else 
            ns <= S_IDLE;
        end if;
     
      when S_END =>  

        hdr_sel <= '0';
        prbs_en <= '0';
        dw_sel <= '0';
        if (en = '0') then
            ns <= S_IDLE;
        else
            ns <= S_END;
        end if;
      when others =>  

        hdr_sel <= '0';
        prbs_en <= '0';
        dw_sel <= '0';
        ns <= S_IDLE;

    end case;
    
  end process;
      
end emp_daqpath_evtgen_fsm_arch;