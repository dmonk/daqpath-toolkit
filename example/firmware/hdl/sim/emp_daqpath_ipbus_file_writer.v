// Rev 26/04/2022 - Output txt file writer entity from IPBUS read transactions

`timescale 1ns / 1ps
module emp_daqpath_ipbus_file_writer(clk, data, dv);

   input wire clk; 
   input wire [31:0] data;
   input wire dv;

   integer 	     outfile, r;
 
   initial
    begin
  		outfile=$fopen("IPBUS_Data_out.txt","w");
  		#10
  		$fclose(outfile);
    end
		
    always @(posedge clk) #1
      if (dv) 
        begin
          outfile=$fopen("IPBUS_Data_out.txt","a");
          $fwrite(outfile, "%h\n", data);
          $fclose(outfile);
        end 
                                 
endmodule
