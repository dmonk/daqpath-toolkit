-- IPBUS input fifos (4x - TS,EV_ID,NW,DW) block for one channel for emp buffer source ex design
-- Rev - 26/04/2022

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.emp_daqpath_types_package.ALL;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.emp_data_types.all;

entity emp_daqpath_emp_buf_ipbus_input_FIFOs is

   generic(
    CHANNEL_NUM : integer := 0
   );

	port(
		clk: in std_logic;
		reset: in std_logic;
		d : in lword;
		ts_fifo_rdata: out std_logic_vector(31 downto 0);
		ts_fifo_re: in std_logic;
		ts_fifo_empty: out std_logic;
		ts_fifo_full: out std_logic;
		id_fifo_rdata: out std_logic_vector(15 downto 0);
		id_fifo_re: in std_logic;
		id_fifo_empty: out std_logic;
		id_fifo_full: out std_logic;
		nw_fifo_rdata: out std_logic_vector(15 downto 0);
		nw_fifo_re: in std_logic;
		nw_fifo_empty: out std_logic;
		nw_fifo_full: out std_logic;
		dw_fifo_rdata: out std_logic_vector(63 downto 0);
		dw_fifo_re: in std_logic;
		dw_fifo_empty: out std_logic;
		dw_fifo_full: out std_logic		
	);
	
end emp_daqpath_emp_buf_ipbus_input_FIFOs;

architecture rtl of emp_daqpath_emp_buf_ipbus_input_FIFOs is

	signal wr_en, int_rd_en : std_logic;

	signal ts_fifo_we : std_logic;
	signal id_fifo_we : std_logic;
	signal nw_fifo_we : std_logic;
	signal dw_fifo_we : std_logic;

	signal ts_fifo_wdata : std_logic_vector(31 downto 0) := (Others => '0');
	signal id_fifo_wdata : std_logic_vector(15 downto 0) := (Others => '0');
	signal nw_fifo_wdata : std_logic_vector(15 downto 0) := (Others => '0');
	signal dw_fifo_wdata : std_logic_vector(63 downto 0) := (Others => '0');

begin

  fsm : entity work.emp_daqpath_ldata_interpreter_fsm
    port map(
      clk => clk,
      rst => reset,
      en => '1',
      d => d,
      ts_fifo_we => ts_fifo_we,
      id_fifo_we => id_fifo_we, 
      nw_fifo_we => nw_fifo_we,
      data_fifo_we => dw_fifo_we
   );

  data_delay_proc : process(clk)
  begin
    if rising_edge(clk) then
      ts_fifo_wdata <= d.data(63 downto 32);
      id_fifo_wdata <= d.data(31 downto 16);
      nw_fifo_wdata <= d.data(15 downto 0);
      dw_fifo_wdata <= d.data(63 downto 0);
    end if;
  end process;
  
  ts_fifo : entity work.fifo_data_32b_bram
    port map (
      rst => reset,
      wr_clk => clk,
      rd_clk => clk,
      din => ts_fifo_wdata,
      wr_en => ts_fifo_we,
      rd_en => ts_fifo_re,
      dout => ts_fifo_rdata,
      full => ts_fifo_full,
      empty => ts_fifo_empty,
      wr_rst_busy => open,
      rd_rst_busy => open
    );	

  id_fifo : entity work.fifo_ctrl_16b_bram
    port map (
      rst => reset,
      wr_clk => clk,
      rd_clk => clk,
      din => id_fifo_wdata,
      wr_en => id_fifo_we,
      rd_en => id_fifo_re,
      dout => id_fifo_rdata,
      full => id_fifo_full,
      empty => id_fifo_empty,
      wr_rst_busy => open,
      rd_rst_busy => open
    );	

  nw_fifo : entity work.fifo_ctrl_16b_bram
    port map (
      rst => reset,
      wr_clk => clk,
      rd_clk => clk,
      din => nw_fifo_wdata,
      wr_en => nw_fifo_we,
      rd_en => nw_fifo_re,
      dout => nw_fifo_rdata,
      full => nw_fifo_full,
      empty => nw_fifo_empty,
      wr_rst_busy => open,
      rd_rst_busy => open
    );	

  dw_fifo : entity work.fifo_data_64b_bram
    port map (
      rst => reset,
      wr_clk => clk,
      rd_clk => clk,
      din => dw_fifo_wdata,
      wr_en => dw_fifo_we,
      rd_en => dw_fifo_re,
      dout => dw_fifo_rdata,
      full => dw_fifo_full,
      empty => dw_fifo_empty,
      wr_rst_busy => open,
      rd_rst_busy => open
    );	


end rtl;
