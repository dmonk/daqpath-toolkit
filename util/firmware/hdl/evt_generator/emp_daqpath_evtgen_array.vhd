-- Top module for internal event generation
-- Rev - 26/04/2022

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use work.emp_daqpath_types_package.ALL;

entity emp_daqpath_evtgen_array is

  generic ( NUM_CHANNELS : integer := 16);
    
  port(
	clk: in std_logic;
	rst: in std_logic;
	trggen_en: in std_logic;
	evtgen_en: in std_logic;
	ch_ctrl0_reg : in ipbus_data_array(NUM_CHANNELS - 1 downto 0);
	ch_ctrl1_reg : in ipbus_data_array(NUM_CHANNELS - 1 downto 0);
	ch_ctrl2_reg : in ipbus_data_array(NUM_CHANNELS - 1 downto 0);
	ch_ctrl3_reg : in ipbus_data_array(NUM_CHANNELS - 1 downto 0);
	ch_evt_cnt_reg : out ipbus_data_array(NUM_CHANNELS - 1 downto 0);
	ch_dw_cnt_reg : out ipbus_data_array(NUM_CHANNELS - 1 downto 0);
	hdr_fifo_wdata: out ipbus_data_array(NUM_CHANNELS-1 downto 0);
	dw_fifo_wdata: out data_fifo_data_array(NUM_CHANNELS-1 downto 0);
	hdr_we: out std_logic_vector(NUM_CHANNELS-1 downto 0);
	dw_we: out std_logic_vector(NUM_CHANNELS-1 downto 0)
  );
	
end emp_daqpath_evtgen_array;

architecture rtl of emp_daqpath_evtgen_array is

  signal trg : std_logic_vector(NUM_CHANNELS-1 downto 0);
  signal header : ipbus_data_array(NUM_CHANNELS - 1 downto 0);
  signal dw_sel : std_logic_vector(NUM_CHANNELS-1 downto 0);
  signal prbs_en : std_logic_vector(NUM_CHANNELS-1 downto 0);
  signal hdr_sel_int : std_logic_vector(NUM_CHANNELS-1 downto 0);
  signal dw_sel_int : std_logic_vector(NUM_CHANNELS-1 downto 0);
  signal ch_ev_dw_cnt_reg : ipbus_data_array(NUM_CHANNELS - 1 downto 0);
  signal int_ch_evt_cnt_reg : ipbus_data_array(NUM_CHANNELS - 1 downto 0);

begin

gen_evtgens : for n in NUM_CHANNELS-1 downto 0 generate

trggens : entity work.emp_daqpath_trggen_fsm
port map(
  clk => clk,
  en => trggen_en,
  rst => rst,
  delta_t => ch_ctrl1_reg(n)(31 downto 0),
  trg => trg(n)
);

evtgens : entity work.emp_daqpath_evtgen_fsm
port map(
  clk => clk,
  en => evtgen_en,
  rst => rst,
  trg => trg(n),
  nw => ch_ctrl3_reg(n)(7 downto 0),
  ne => ch_ctrl2_reg(n)(15 downto 0),
  header => hdr_fifo_wdata(n),
  hdr_sel => hdr_sel_int(n),
  prbs_en => prbs_en(n),
  dw_sel => dw_sel_int(n)
);	

evtcnt : entity work.emp_daqpath_clk_cnt
generic map(
  NB => 32 )  
port map(
  clk => clk,
  en => hdr_sel_int(n),
  rst => rst, 
  cnt_out => int_ch_evt_cnt_reg(n)
);

dwcnt : entity work.emp_daqpath_clk_cnt
generic map(
  NB => 16 )  
port map
  (clk => clk,
   en => dw_sel_int(n),
   rst => rst, 
   cnt_out => ch_dw_cnt_reg(n)
  );
 
ev_dwcnt : entity work.emp_daqpath_clk_cnt
generic map(
  NB => 32 )  
port map
  (clk => clk,
   en => dw_sel_int(n),
   rst => hdr_sel_int(n), 
   cnt_out => ch_ev_dw_cnt_reg(n)
  );
 
 dw_fifo_wdata(n) <= x"0000" & std_logic_vector(to_unsigned(n, 16)) & int_ch_evt_cnt_reg(n)(15 downto 0) & ch_ev_dw_cnt_reg(n)(15 downto 0);
 ch_evt_cnt_reg(n) <= int_ch_evt_cnt_reg(n);
 
 end generate gen_evtgens;

process (clk)
  begin
    if rising_edge(clk) then
      hdr_we <= hdr_sel_int;
      dw_we <= dw_sel_int;
  end if;
end process;

end rtl;
