-- Main FSM for readout controls (start,end...)  
-- Rev - 07/07/2022

--  version 2 with channel mask

library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity emp_daqpath_main_fsm is

  generic (NUM_CHANNELS : integer := 4);

  port (
    clk : IN STD_LOGIC;
    en : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    token_in : IN STD_LOGIC;
    empty : IN STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);    
    ch_mask : IN STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    token_out : OUT STD_LOGIC;
    current_id : OUT STD_LOGIC_VECTOR(15 downto 0)
  );

end emp_daqpath_main_fsm;


ARCHITECTURE emp_daqpath_main_fsm_arch of emp_daqpath_main_fsm is

type state_type is (S_IDLE, S_START, S_WAIT, S_LATENCY);
signal cs: state_type;
signal ns: state_type;

signal cur_id_cnt : integer := 0;
signal cur_id_cnt_en : std_logic;

signal empty_value : integer;
signal empty_masked, all_empty_ref : STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);   -- all_empty_ref for timeout -- not used yet

begin

all_empty_ref <= NOT ch_mask;  -- to trigger timeout conditions when event is arrived in some channels but not in all

empty_masked_proc : process(empty, ch_mask)
begin
  for I in 0 to NUM_CHANNELS-1 loop
    empty_masked(I) <= empty(I) and (not ch_mask(I));  -- if ch_mask(I) = 0 empty_masked <= empty, else empty_masked <= 0 
  end loop;
end process;
empty_value <= TO_INTEGER(unsigned(empty_masked));

current_id <= std_logic_vector(to_unsigned(cur_id_cnt, 16));

cnt_proc : process (cur_id_cnt_en,clk,rst)
begin
  if (rst = '1') then
    cur_id_cnt <= 0;
  elsif rising_edge(clk) and (cur_id_cnt_en = '1') then
    cur_id_cnt <= cur_id_cnt + 1;
  end if;
end process;

FSM_regs: process (ns,rst,clk)
  begin
    if (rst = '1') then
      cs <= S_IDLE;
    elsif rising_edge(clk) then
      cs <= ns;
    end if;
end process;

FSM_logic: process (empty,empty_value,token_in,en,cs)
  begin
    case cs is
    
      when S_IDLE =>  

  token_out <= '0';
  
  if (empty_value = 0) and (en = '1') then
    cur_id_cnt_en  <= '1';
    ns <= S_START;
  else
    cur_id_cnt_en  <= '0';
    ns <= S_IDLE;
  end if;  

      when S_START =>

  token_out <= '1';
  cur_id_cnt_en  <= '0';
    
    ns <= S_WAIT;
    
      when S_WAIT =>

  token_out <= '0';
  cur_id_cnt_en  <= '0';

        if (token_in = '1') then        
          ns <= S_LATENCY;
          else
          ns <= S_WAIT;
        end if;
        
      when S_LATENCY =>

   token_out <= '0';
   cur_id_cnt_en  <= '0';

          ns <= S_IDLE;
     
      when others =>  

  token_out <= '0';
  cur_id_cnt_en  <= '0';

    ns <= S_IDLE;      
    
      end case;

  end process;
      
end emp_daqpath_main_fsm_arch;