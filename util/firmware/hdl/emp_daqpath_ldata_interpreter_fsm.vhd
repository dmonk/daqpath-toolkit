-- FSM for 1st level fifos filling from ldata words
-- Rev - 26/04/2022

library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use work.emp_daqpath_types_package.ALL;
use work.emp_data_types.all;


entity emp_daqpath_ldata_interpreter_FSM  is

  port (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    en : IN STD_LOGIC;
    d : in lword;
    ts_fifo_we : OUT STD_LOGIC;
    id_fifo_we : OUT STD_LOGIC;
    nw_fifo_we : OUT STD_LOGIC;
    data_fifo_we : OUT STD_LOGIC
  );

end emp_daqpath_ldata_interpreter_FSM ;


ARCHITECTURE emp_daqpath_ldata_interpreter_FSM_arch of emp_daqpath_ldata_interpreter_FSM is

type state_type is (S_IDLE, S_WRITE);
signal cs: state_type;
signal ns: state_type;

signal nw_cnt : integer;
signal nw_cnt_en, nw_cnt_ld : std_logic;
signal data_fifo_we_int, nw_fifo_we_int, ts_fifo_we_int, id_fifo_we_int : std_logic;

begin

  we_reg_proc : process(clk)
  begin
    if rising_edge(clk) then
      data_fifo_we <= data_fifo_we_int;
      id_fifo_we <= id_fifo_we_int;
      nw_fifo_we <= nw_fifo_we_int;
      ts_fifo_we <= ts_fifo_we_int;
    end if;
  end process;

  nw_cnt_proc: process (nw_cnt,nw_cnt_en,nw_cnt_ld,rst,clk)
    begin
      if (rst = '1') then 
        nw_cnt <= 1;
      elsif (nw_cnt_en = '1') and rising_edge(clk) then
        if (nw_cnt_ld = '1') then
          nw_cnt <= to_integer(unsigned(d.data(15 downto 0)));
        else
          nw_cnt <= nw_cnt - 1;
        end if;
      end if;        
  end process;

  FSM_regs: process (ns,rst,clk)
    begin
      if (rst = '1') then
        cs <= S_IDLE;
      elsif rising_edge(clk) then
        cs <= ns;
      end if;
  end process;
  
  FSM_logic: process (en,nw_cnt,d,cs)
    begin
      case cs is
    
        when S_IDLE => 

      data_fifo_we_int <= '0';
      
      if (en = '1' and d.valid = '1' and d.strobe = '1') then
        ts_fifo_we_int <= '1';
        id_fifo_we_int <= '1';
        nw_fifo_we_int <= '1';
        nw_cnt_en <= '1';
        nw_cnt_ld <= '1';        
        ns <= S_WRITE;
       else
        ts_fifo_we_int <= '0';
        id_fifo_we_int <= '0';
        nw_fifo_we_int <= '0';
        nw_cnt_en <= '0';
        nw_cnt_ld <= '0';
        ns <= S_IDLE;
      end if;  

        when S_WRITE =>
      
      ts_fifo_we_int <= '0';
      id_fifo_we_int <= '0';
      nw_fifo_we_int <= '0';
      nw_cnt_ld <= '0';
  
      if (nw_cnt > 0 and d.valid='1') then
        nw_cnt_en <= '1';
        data_fifo_we_int <= '1';
        ns <= S_WRITE;
      elsif (nw_cnt > 0 and d.valid='0') then
        nw_cnt_en <= '0';
        data_fifo_we_int <= '0';
        ns <= S_WRITE; 
      else
        nw_cnt_en <= '0';
        data_fifo_we_int <= '0';
        ns <= S_IDLE;
      end if;  
    
         when others =>  

      ts_fifo_we_int <= '0';
      id_fifo_we_int <= '0';
      nw_fifo_we_int <= '0';
      nw_cnt_en <= '0';
      nw_cnt_ld <= '0';
      data_fifo_we_int <= '0';
      ns <= S_IDLE;      
    
      end case;

  end process;
      
end emp_daqpath_ldata_interpreter_FSM_arch;
