-- IPBUS registers for daqpath channels config/monitoring
-- Rev - 26/04/2022

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use work.emp_daqpath_types_package.ALL;
use work.ipbus.all;
use work.ipbus_reg_types.all;

entity emp_daqpath_chan_ipbus_csregs is

    generic ( 
      NUM_CHANNELS : integer := 16;
      NUM_REGISTERS : integer := 4
    );
    
    port ( 

      ipb_clk : in std_logic;
      dp_clk : in std_logic;
      reset : in std_logic;
      ipbus_in: in ipb_wbus;
      ipbus_out: out ipb_rbus;
      creg : out ipbus_data_array2(NUM_CHANNELS-1 downto 0, NUM_REGISTERS-1 downto 0);
      sreg : in ipbus_data_array2(NUM_CHANNELS-1 downto 0, NUM_REGISTERS-1 downto 0)
    );

end emp_daqpath_chan_ipbus_csregs;

architecture emp_daqpath_chan_ipbus_csregs_Arch of emp_daqpath_chan_ipbus_csregs is

signal    ipbw : ipb_wbus_array(NUM_CHANNELS-1 downto 0);
signal    ipbr : ipb_rbus_array(NUM_CHANNELS-1 downto 0); 

type ipb_reg_v_array is array(natural range <>) of ipb_reg_v(NUM_REGISTERS-1 downto 0);
signal ch_creg : ipb_reg_v_array(NUM_CHANNELS-1 downto 0);
signal ch_sreg : ipb_reg_v_array(NUM_CHANNELS-1 downto 0);

begin

fabric: entity work.ipbus_fabric_simple
    generic map(
      NSLV => NUM_CHANNELS,
      STROBE_GAP => false,
      DECODE_BASE => 8,
      DECODE_BITS => 8
    )
    port map(
      ipb_in => ipbus_in,
      ipb_out => ipbus_out,
      ipb_to_slaves => ipbw,
      ipb_from_slaves => ipbr
    );

gen_registers : for n in NUM_CHANNELS-1 downto 0 generate
    
end generate gen_registers;

ipb_channel_regs : for i in (NUM_CHANNELS - 1) downto 0 generate
  begin
    ch_csregs : entity work.ipbus_syncreg_v
      generic map(
        N_CTRL => NUM_REGISTERS,
        N_STAT => NUM_REGISTERS
      )
      port map(
        clk        => ipb_clk,
        rst        => reset,    
        ipb_in     => ipbw(i),
        ipb_out    => ipbr(i),
        slv_clk    => dp_clk,
        d          => ch_sreg(i),
        q          => ch_creg(i)
      );
end generate; 

process (ch_creg)
begin
	for J in 0 to NUM_CHANNELS-1 loop
  	  for I in 0 to NUM_REGISTERS-1 loop
  	    creg(J,I) <= ch_creg(J)(I);
  	  end loop;
	end loop;
end process;

process (sreg)
begin
	for J in 0 to NUM_CHANNELS-1 loop
  	  for I in 0 to NUM_REGISTERS-1 loop
  	    ch_sreg(J)(I) <= sreg(J,I);
  	  end loop;
	end loop;
end process;

end emp_daqpath_chan_ipbus_csregs_Arch;
