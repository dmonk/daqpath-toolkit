-- IPBUS input fifos (4x - TS,EV_ID,NW,DW) block for one channel
-- Rev - 26/04/2022

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.ipbus_reg_types.all;

entity emp_daqpath_ipbus_input_FIFOs is

   generic(
    CHANNEL_NUM : integer := 0
   );

	port(
		ipb_clk: in std_logic;
		rx_clk: in std_logic;
		reset: in std_logic;
		ipbus_in: in ipb_wbus;
		ipbus_out: out ipb_rbus;
		ts_fifo_rdata: out std_logic_vector(31 downto 0);
		ts_fifo_re: in std_logic;
		ts_fifo_empty: out std_logic;
		ts_fifo_full: out std_logic;
		id_fifo_rdata: out std_logic_vector(15 downto 0);
		id_fifo_re: in std_logic;
		id_fifo_empty: out std_logic;
		id_fifo_full: out std_logic;
		nw_fifo_rdata: out std_logic_vector(15 downto 0);
		nw_fifo_re: in std_logic;
		nw_fifo_empty: out std_logic;
		nw_fifo_full: out std_logic;
		dw_fifo_rdata: out std_logic_vector(63 downto 0);
		dw_fifo_re: in std_logic;
		dw_fifo_empty: out std_logic;
		dw_fifo_full: out std_logic		
	);
	
end emp_daqpath_ipbus_input_FIFOs;

architecture rtl of emp_daqpath_ipbus_input_FIFOs is

	signal wr_clk, rd_clk : std_logic;
	signal wr_en, int_rd_en : std_logic;

	signal ts_fifo_sel, ts_fifo_we : std_logic;
	signal id_fifo_sel, id_fifo_we : std_logic;
	signal nw_fifo_sel, nw_fifo_we : std_logic;
	signal dw_l_fifo_sel, dw_l_fifo_we : std_logic;
	signal dw_h_fifo_sel, dw_h_fifo_we : std_logic;

	signal ts_fifo_wdata : std_logic_vector(31 downto 0);
	signal id_fifo_wdata : std_logic_vector(15 downto 0);
	signal nw_fifo_wdata : std_logic_vector(15 downto 0);
	signal int_ts_fifo_rdata : std_logic_vector(31 downto 0);
	signal int_id_fifo_rdata : std_logic_vector(31 downto 0);
	signal int_nw_fifo_rdata : std_logic_vector(31 downto 0);
	signal dw_fifo_wdata, int_dw_fifo_rdata : std_logic_vector(63 downto 0);

	signal dw_l_fifo_empty, dw_l_fifo_full : std_logic;
	signal dw_h_fifo_empty, dw_h_fifo_full : std_logic;
    	
	signal wr_data, int_rd_data: std_logic_vector(15 downto 0);

begin

  wr_clk <= ipb_clk;
  rd_clk <= rx_clk;

  ts_fifo_wdata <= ipbus_in.ipb_wdata;
  id_fifo_wdata <= ipbus_in.ipb_wdata(15 downto 0);
  nw_fifo_wdata <= ipbus_in.ipb_wdata(15 downto 0);
  dw_fifo_wdata(31 downto 0) <= ipbus_in.ipb_wdata;
  dw_fifo_wdata(63 downto 32) <= ipbus_in.ipb_wdata;

  ts_fifo_sel <= '1' when ipbus_in.ipb_addr(2 downto 0) = "100" else '0';
  id_fifo_sel <= '1' when ipbus_in.ipb_addr(2 downto 0) = "011" else '0';
  nw_fifo_sel <= '1' when ipbus_in.ipb_addr(2 downto 0) = "010" else '0';
  dw_h_fifo_sel <= '1' when ipbus_in.ipb_addr(2 downto 0) = "001" else '0';
  dw_l_fifo_sel <= '1' when ipbus_in.ipb_addr(2 downto 0) = "000" else '0';
    
  ts_fifo_we <= ipbus_in.ipb_strobe and ipbus_in.ipb_write and ts_fifo_sel;
  id_fifo_we <= ipbus_in.ipb_strobe and ipbus_in.ipb_write and id_fifo_sel;
  nw_fifo_we <= ipbus_in.ipb_strobe and ipbus_in.ipb_write and nw_fifo_sel;
  dw_h_fifo_we <= ipbus_in.ipb_strobe and ipbus_in.ipb_write and dw_h_fifo_sel;
  dw_l_fifo_we <= ipbus_in.ipb_strobe and ipbus_in.ipb_write and dw_l_fifo_sel;

  ts_fifo_rdata <= int_ts_fifo_rdata;

  id_fifo_rdata <= int_id_fifo_rdata(15 downto 0);

  nw_fifo_rdata <= int_nw_fifo_rdata(15 downto 0);

  dw_fifo_rdata(31 downto 0) <= int_dw_fifo_rdata(31 downto 0);

  dw_fifo_rdata(63 downto 32) <= int_dw_fifo_rdata(63 downto 32);
    
  int_id_fifo_rdata(31 downto 16) <= "0000000000000000";
  int_nw_fifo_rdata(31 downto 16) <= "0000000000000000";
    
  ipbus_out.ipb_rdata <= ipbus_in.ipb_wdata;                       
  ipbus_out.ipb_ack <= ipbus_in.ipb_strobe;
  ipbus_out.ipb_err <= '0';

  dw_fifo_empty <= dw_l_fifo_empty and dw_h_fifo_empty;
  dw_fifo_full <= dw_l_fifo_full and dw_h_fifo_full;
    
  ts_fifo : entity work.fifo_data_32b_bram
    port map (
      rst => reset,
      wr_clk => wr_clk,
      rd_clk => rd_clk,
      din => ts_fifo_wdata,
      wr_en => ts_fifo_we,
      rd_en => ts_fifo_re,
      dout => int_ts_fifo_rdata,
      full => ts_fifo_full,
      empty => ts_fifo_empty,
      wr_rst_busy => open,
      rd_rst_busy => open
    );	

  id_fifo : entity work.fifo_ctrl_16b_bram
    port map (
      rst => reset,
      wr_clk => wr_clk,
      rd_clk => rd_clk,
      din => id_fifo_wdata,
      wr_en => id_fifo_we,
      rd_en => id_fifo_re,
      dout => int_id_fifo_rdata(15 downto 0),
      full => id_fifo_full,
      empty => id_fifo_empty,
      wr_rst_busy => open,
      rd_rst_busy => open
    );	

  nw_fifo : entity work.fifo_ctrl_16b_bram
    port map (
      rst => reset,
      wr_clk => wr_clk,
      rd_clk => rd_clk,
      din => nw_fifo_wdata,
      wr_en => nw_fifo_we,
      rd_en => nw_fifo_re,
      dout => int_nw_fifo_rdata(15 downto 0),
      full => nw_fifo_full,
      empty => nw_fifo_empty,
      wr_rst_busy => open,
      rd_rst_busy => open
    );	

  dw_l_fifo : entity work.fifo_data_32b_bram
    port map (
      rst => reset,
      wr_clk => wr_clk,
      rd_clk => rd_clk,
      din => dw_fifo_wdata(31 downto 0),
      wr_en => dw_l_fifo_we,
      rd_en => dw_fifo_re,
      dout => int_dw_fifo_rdata(31 downto 0),
      full => dw_l_fifo_full,
      empty => dw_l_fifo_empty,
      wr_rst_busy => open,
      rd_rst_busy => open
    );	

  dw_h_fifo : entity work.fifo_data_32b_bram
    port map (
      rst => reset,
      wr_clk => wr_clk,
      rd_clk => rd_clk,
      din => dw_fifo_wdata(63 downto 32),
      wr_en => dw_h_fifo_we,
      rd_en => dw_fifo_re,
      dout => int_dw_fifo_rdata(63 downto 32),
      full => dw_h_fifo_full,
      empty => dw_h_fifo_empty,
      wr_rst_busy => open,
      rd_rst_busy => open
    );	

end rtl;
