-- IPBUS output fifos block (3x 32b wide fifos per output link - DW_high,DW_low,ID_NW)  
-- Rev - 13/09/2021

library IEEE;
Library xpm;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use xpm.vcomponents.all;

entity emp_daqpath_ipbus_output_FIFOs is

  generic(
    CHANNEL_NUM : integer := 0;
    Data_fifo_word_depth : integer := 32768
  );

	port(
		dp_clk: in std_logic;
		ipb_clk: in std_logic;
		reset: in std_logic;
		ipbus_in: in ipb_wbus;
		ipbus_out: out ipb_rbus;
		data_fifo_wdata: in std_logic_vector(63 downto 0);
		data_fifo_we: in std_logic;
		data_fifo_empty: out std_logic;
		data_fifo_full: out std_logic;
		idnw_fifo_wdata: in std_logic_vector(31 downto 0);
		idnw_fifo_we: in std_logic;
		idnw_fifo_empty: out std_logic;
		idnw_fifo_full: out std_logic
	);
	
end emp_daqpath_ipbus_output_FIFOs;

architecture rtl of emp_daqpath_ipbus_output_FIFOs is

	signal wr_clk, rd_clk : std_logic;
	signal wr_en, int_rd_en : std_logic;

	signal data_l_fifo_sel, data_h_fifo_sel, idnw_fifo_sel : std_logic;
	signal data_l_fifo_re, data_h_fifo_re, idnw_fifo_re : std_logic;

	signal data_l_fifo_empty, data_h_fifo_empty : std_logic;
	signal data_l_fifo_full, data_h_fifo_full : std_logic;
   	
	signal data_fifo_rdata: std_logic_vector(63 downto 0);
	signal idnw_fifo_rdata: std_logic_vector(31 downto 0);

begin

  wr_clk <= dp_clk;
  rd_clk <= ipb_clk;

  data_l_fifo_sel <= (not ipbus_in.ipb_addr(1)) and (not ipbus_in.ipb_addr(0));
  data_h_fifo_sel <= (not ipbus_in.ipb_addr(1)) and (ipbus_in.ipb_addr(0));
  idnw_fifo_sel <= ipbus_in.ipb_addr(1);
  
  data_l_fifo_re <= ipbus_in.ipb_strobe and (not ipbus_in.ipb_write) and data_l_fifo_sel;
  data_h_fifo_re <= ipbus_in.ipb_strobe and (not ipbus_in.ipb_write) and data_h_fifo_sel;
  idnw_fifo_re <= ipbus_in.ipb_strobe and (not ipbus_in.ipb_write) and idnw_fifo_sel;
    
  ipbus_out.ipb_rdata <= data_fifo_rdata(63 downto 32) when data_h_fifo_sel = '1' else
	                       data_fifo_rdata(31 downto 0) when data_l_fifo_sel = '1' else
	                       idnw_fifo_rdata(31 downto 0) when idnw_fifo_sel = '1' else
	                       (others => '0');
	                       
  ipbus_out.ipb_ack <= ipbus_in.ipb_strobe;
  ipbus_out.ipb_err <= '0';

--  --data_h_fifo : entity work.fifo_data_32b_bram
--  data_h_fifo : entity work.fifo_data_32k32b_bram
--    port map (
--      rst => reset,
--      wr_clk => wr_clk,
--      rd_clk => rd_clk,
--      din => data_fifo_wdata(63 downto 32),
--      wr_en => data_fifo_we,
--      rd_en => data_h_fifo_re,
--      dout => data_fifo_rdata(63 downto 32),
--      full => data_h_fifo_full,
--      empty => data_h_fifo_empty,
--      wr_rst_busy => open,
--      rd_rst_busy => open
--    );	

--  --data_l_fifo : entity work.fifo_data_32b_bram
--  data_l_fifo : entity work.fifo_data_32k32b_bram
--    port map (
--      rst => reset,
--      wr_clk => wr_clk,
--      rd_clk => rd_clk,
--      din => data_fifo_wdata(31 downto 0),
--      wr_en => data_fifo_we,
--      rd_en => data_l_fifo_re,
--      dout => data_fifo_rdata(31 downto 0),
--      full => data_l_fifo_full,
--      empty => data_l_fifo_empty,
--      wr_rst_busy => open,
--      rd_rst_busy => open
--    );

--idnw_fifo : entity work.fifo_data_32b_bram
--    port map (
--      rst => reset,
--      wr_clk => wr_clk,
--      rd_clk => rd_clk,
--      din => idnw_fifo_wdata(31 downto 0),
--      wr_en => idnw_fifo_we,
--      rd_en => idnw_fifo_re,
--      dout => idnw_fifo_rdata(31 downto 0),
--      full => idnw_fifo_full,
--      empty => idnw_fifo_empty,
--      wr_rst_busy => open,
--      rd_rst_busy => open
--    );

  data_h_fifo : xpm_fifo_async
    generic map (
      CDC_SYNC_STAGES => 2, -- DECIMAL
      DOUT_RESET_VALUE => "0", -- String
      ECC_MODE => "no_ecc", -- String
      FIFO_MEMORY_TYPE => "block", -- String
      FIFO_READ_LATENCY => 0, -- DECIMAL  --first word fall through
      FIFO_WRITE_DEPTH => Data_fifo_word_depth, -- DECIMAL
      FULL_RESET_VALUE => 0, -- DECIMAL
      PROG_EMPTY_THRESH => 5, -- DECIMAL
      PROG_FULL_THRESH => 5, -- DECIMAL
      RD_DATA_COUNT_WIDTH => 1, -- DECIMAL
      READ_DATA_WIDTH => 32, -- DECIMAL
      READ_MODE => "fwft", -- String
      RELATED_CLOCKS => 0, -- DECIMAL
      USE_ADV_FEATURES => "0000", -- String
      WAKEUP_TIME => 0, -- DECIMAL
      WRITE_DATA_WIDTH => 32, -- DECIMAL
      WR_DATA_COUNT_WIDTH => 1 -- DECIMAL
     )
     port map (
       rst => reset,
       wr_clk => wr_clk,
       rd_clk => rd_clk,
       din => data_fifo_wdata(63 downto 32),
       wr_en => data_fifo_we,
       rd_en => data_h_fifo_re,
       dout => data_fifo_rdata(63 downto 32),
       full => data_h_fifo_full,
       empty => data_h_fifo_empty,
       sleep => '0',
       injectdbiterr => '0',
       injectsbiterr => '0',
       wr_rst_busy => open,
       rd_rst_busy => open
     );
   
   data_l_fifo : xpm_fifo_async
    generic map (
      CDC_SYNC_STAGES => 2, -- DECIMAL
      DOUT_RESET_VALUE => "0", -- String
      ECC_MODE => "no_ecc", -- String
      FIFO_MEMORY_TYPE => "block", -- String
      FIFO_READ_LATENCY => 0, -- DECIMAL  --first word fall through
      FIFO_WRITE_DEPTH => Data_fifo_word_depth, -- DECIMAL
      FULL_RESET_VALUE => 0, -- DECIMAL
      PROG_EMPTY_THRESH => 5, -- DECIMAL
      PROG_FULL_THRESH => 5, -- DECIMAL
      RD_DATA_COUNT_WIDTH => 1, -- DECIMAL
      READ_DATA_WIDTH => 32, -- DECIMAL
      READ_MODE => "fwft", -- String
      RELATED_CLOCKS => 0, -- DECIMAL
      USE_ADV_FEATURES => "0000", -- String
      WAKEUP_TIME => 0, -- DECIMAL
      WRITE_DATA_WIDTH => 32, -- DECIMAL
      WR_DATA_COUNT_WIDTH => 1 -- DECIMAL
     )
     port map (
       rst => reset,
       wr_clk => wr_clk,
       rd_clk => rd_clk,
       din => data_fifo_wdata(31 downto 0),
       wr_en => data_fifo_we,
       rd_en => data_l_fifo_re,
       dout => data_fifo_rdata(31 downto 0),
       full => data_l_fifo_full,
       empty => data_l_fifo_empty,
       sleep => '0',
       injectdbiterr => '0',
       injectsbiterr => '0',
       wr_rst_busy => open,
       rd_rst_busy => open
     );
     
  idnw_fifo : xpm_fifo_async
    generic map (
      CDC_SYNC_STAGES => 2, -- DECIMAL
      DOUT_RESET_VALUE => "0", -- String
      ECC_MODE => "no_ecc", -- String
      FIFO_MEMORY_TYPE => "block", -- String
      FIFO_READ_LATENCY => 0, -- DECIMAL  --first word fall through
      FIFO_WRITE_DEPTH => 1024, -- DECIMAL
      FULL_RESET_VALUE => 0, -- DECIMAL
      PROG_EMPTY_THRESH => 5, -- DECIMAL
      PROG_FULL_THRESH => 5, -- DECIMAL
      RD_DATA_COUNT_WIDTH => 1, -- DECIMAL
      READ_DATA_WIDTH => 32, -- DECIMAL
      READ_MODE => "fwft", -- String
      RELATED_CLOCKS => 0, -- DECIMAL
      USE_ADV_FEATURES => "0000", -- String
      WAKEUP_TIME => 0, -- DECIMAL
      WRITE_DATA_WIDTH => 32, -- DECIMAL
      WR_DATA_COUNT_WIDTH => 1 -- DECIMAL
     )
     port map (
       rst => reset,
       wr_clk => wr_clk,
       rd_clk => rd_clk,
       din => idnw_fifo_wdata(31 downto 0),
       wr_en => idnw_fifo_we,
       rd_en => idnw_fifo_re,
       dout => idnw_fifo_rdata(31 downto 0),
       full => idnw_fifo_full,
       empty => idnw_fifo_empty,
       sleep => '0',
       injectdbiterr => '0',
       injectsbiterr => '0',
       wr_rst_busy => open,
       rd_rst_busy => open
     );  	

  data_fifo_empty <= data_l_fifo_empty and data_h_fifo_empty;
  data_fifo_full <= data_l_fifo_full and data_h_fifo_full;

end rtl;
