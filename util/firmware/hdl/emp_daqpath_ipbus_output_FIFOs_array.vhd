-- IPBUS output fifos array block  
-- Rev - 13/09/2021

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.emp_daqpath_types_package.ALL;
use work.ipbus.all;
use work.ipbus_reg_types.all;

entity emp_daqpath_ipbus_output_FIFOs_array is

    generic ( 
      NUM_CHANNELS : integer := 1;
      Data_fifo_word_depth : integer := 32768
      -- Default is 32k fifos (2 fifo with 32b words each for low and high parts)
      );
    
    port ( 
      dp_clk : in std_logic;
      ipb_clk : in std_logic;
      rst : in std_logic;
      en : in std_logic;
      ipb_in: in ipb_wbus;
      ipb_out: out ipb_rbus;
      data_fifo_wdata : in data_fifo_data_array(NUM_CHANNELS-1 downto 0);
      data_fifo_we : in STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
      data_fifo_empty : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
      data_fifo_full : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
      idnw_fifo_wdata : in ipbus_data_array(NUM_CHANNELS-1 downto 0);
      idnw_fifo_we : in STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
      idnw_fifo_empty : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
      idnw_fifo_full : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0)
    );

end emp_daqpath_ipbus_output_FIFOs_array;

architecture emp_daqpath_ipbus_output_FIFOs_array_Arch of emp_daqpath_ipbus_output_FIFOs_array is

signal    ipb_to_slaves : ipb_wbus_array(NUM_CHANNELS-1 downto 0);
signal    ipb_from_slaves : ipb_rbus_array(NUM_CHANNELS-1 downto 0); 

begin

ipbus_fabric : entity work.ipbus_fabric_simple
  generic map(
    NSLV => NUM_CHANNELS,
    STROBE_GAP => false,
    DECODE_BASE => 16,
    DECODE_BITS => 4)
  port map (
    ipb_in => ipb_in,
    ipb_out => ipb_out,
    ipb_to_slaves => ipb_to_slaves,
    ipb_from_slaves => ipb_from_slaves
  );

gen_fifos : 
for n in NUM_CHANNELS-1 downto 0 generate

ipbus_fifos : entity work.emp_daqpath_ipbus_output_FIFOs
  generic map (n, Data_fifo_word_depth)
  port map (
    dp_clk => dp_clk,
    ipb_clk => ipb_clk,
    reset => rst,
    ipbus_in => ipb_to_slaves(n),
    ipbus_out => ipb_from_slaves(n),
    data_fifo_wdata => data_fifo_wdata(n),
  	data_fifo_we => data_fifo_we(n),
  	data_fifo_empty => data_fifo_empty(n),
  	data_fifo_full => data_fifo_full(n),
  	idnw_fifo_wdata => idnw_fifo_wdata(n),
  	idnw_fifo_we => idnw_fifo_we(n),
  	idnw_fifo_empty => idnw_fifo_empty(n),
  	idnw_fifo_full => idnw_fifo_full(n)
  );
    
 end generate gen_fifos;

end emp_daqpath_ipbus_output_FIFOs_array_Arch;
