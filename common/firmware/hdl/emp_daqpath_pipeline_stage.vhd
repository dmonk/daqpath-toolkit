-- Base pipeline register stage block for pipelined version
-- Rev - 26/04/2022

library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity emp_daqpath_pipeline_stage is

  generic(
    NBIT : integer := 84
  );
  port(
    clk : in std_logic;
    rst : in std_logic; 
    din : in std_logic_vector(NBIT-1 downto 0);
    dout : out std_logic_vector(NBIT-1 downto 0)
  );

end emp_daqpath_pipeline_stage;

architecture behavioral of emp_daqpath_pipeline_Stage is

begin
  
Pipeline_Stage_proc : process (clk,rst,din)
  
  begin
    
    if (rst = '1') then
        dout <= (others => '0');
    elsif rising_edge(clk) then
        dout <= din;
    end if;
    
  end process;
  
end behavioral;
