-- EMP standard payload containing the top entity of a N(=16 default) channel (empbutler input/ipbus output) daqpath example design
-- Rev. 16/09/2022

-- Dave Newbold, July 2013
-- Adding slink outputs and ipbus config : Kristian Hahn, 2022

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL; -- or_reduce
use IEEE.NUMERIC_STD.ALL;

use work.ipbus.all;
use work.emp_data_types.all;
use work.emp_project_decl.all;

use work.emp_device_decl.all;
use work.emp_ttc_decl.all;

use work.emp_slink_types.all;
use work.emp_daqpath_user_package.all;
use work.ipbus_decode_emp_payload.all;


entity emp_payload is

  port(
    clk          : in  std_logic;        -- ipbus signals
    rst          : in  std_logic;
    ipb_in       : in  ipb_wbus;
    ipb_out      : out ipb_rbus;
    clk40        : in  std_logic;        
    clk_payload  : in  std_logic_vector(2 downto 0);
    rst_payload  : in  std_logic_vector(2 downto 0);
    clk_p        : in  std_logic;        -- data clock
    rst_loc      : in  std_logic_vector(N_REGION - 1 downto 0);
    clken_loc    : in  std_logic_vector(N_REGION - 1 downto 0);
    ctrs         : in  ttc_stuff_array;
    bc0          : out std_logic;
    d            : in  ldata(4 * N_REGION - 1 downto 0);  -- data in
    q            : out ldata(4 * N_REGION - 1 downto 0);  -- data out
    gpio         : out std_logic_vector(29 downto 0);  -- IO to mezzanine connector
    gpio_en      : out std_logic_vector(29 downto 0);  -- IO to mezzanine connector (three-state enables)
    slink_q      : out slink_input_data_quad_array(SLINK_MAX_QUADS-1 downto 0);
    backpressure : in std_logic_vector(SLINK_MAX_QUADS-1 downto 0)
  );

end emp_payload;

architecture rtl of emp_payload is

--Ipbus signals
signal    ipbw : ipb_wbus_array(N_SLAVES-1 downto 0);
signal    ipbr : ipb_rbus_array(N_SLAVES-1 downto 0);
-----------------------------------------------------

signal   slink_i              : slink_input_data_array;
signal   backpressure_dummy   : std_logic;
signal   backpressure_daqpath : std_logic;

constant N_MAIN_REGS : integer := 8;
constant N_CH_REGS : integer := 4;
constant N_LINKS : integer := 1;

signal   data_out : STD_LOGIC_VECTOR(63 downto 0);
signal   data_we_out : STD_LOGIC;
signal   idnw_out : STD_LOGIC_VECTOR(31 downto 0);
signal   idnw_we_out : STD_LOGIC;

signal   slink_data : STD_LOGIC_VECTOR(63 downto 0);
signal   slink_dv : STD_LOGIC;
signal   slink_start : STD_LOGIC;
signal   slink_end : STD_LOGIC;

signal   q_int : ldata(4 * N_REGION - 1 downto 0) ;
signal   d_int : ldata(N_CHAN-1 downto 0) ;

-- global counters
signal ge_id                  : std_logic_vector(SLINK_INPUT_TRIGGER_GLOBAL_EVENT_ID_WIDTH-1 downto 0) := (others => '0');
signal bx_id                  : std_logic_vector(SLINK_INPUT_TRIGGER_BX_ID_WIDTH-1 downto 0) := (others => '0');
signal orbit_id               : std_logic_vector(SLINK_INPUT_TRIGGER_ORBIT_ID_WIDTH-1 downto 0) := (others => '0');

begin

  -- Un-used stuff ----------
  q <= q_int;
  bc0 <= '0';
  gpio    <= (others => '0');
  gpio_en <= (others => '0');
  ---------------------------
  
  --------------------------------------------------------------
  -- Main ipbus fabric
  -------------------------------------------------------------- 
  fabric: entity work.ipbus_fabric_sel
    generic map(
      NSLV => N_SLAVES,
      SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      sel => ipbus_sel_emp_payload(ipb_in.ipb_addr),
      ipb_to_slaves => ipbw,
      ipb_from_slaves => ipbr
    );
  
  -- Slink backpressure
  backpressure_dummy   <= or_reduce(backpressure(SLINK_MAX_QUADS-1 downto 1));
  backpressure_daqpath <= backpressure(0);
  
  -- Dummy gen for Slink quads >0 (Only using SLINK quad 0 with 1 daqpath module)
  -- Only used to retrive physical_id and l1_type from emp_slink_generator ipbus regs
  dummy : entity work.emp_slink_generator
    generic map ( channel_mask => x"F" )
    port map (
      ipb_clk      => clk,
      ipb_rst      => rst,
      ipb_in       => ipbw(N_SLV_GENERATOR),
      ipb_out      => ipbr(N_SLV_GENERATOR),

      rst_p        => '0',
      clk_p        => clk_p,

      data_q       => slink_i,
      backpressure => backpressure_dummy
      );

  slink_q_gen : for q in SLINK_MAX_QUADS-1 downto 1 generate
    begin
      slink_q(q) <= slink_i;
    end generate;
  
  --Ldata ports assignement 
  gen : for i in 3 downto 0 generate

    constant idh : integer := i * 4 + 3;
    constant idl : integer := i * 4;
    
  begin

    process(clk_p)
    begin
      if rising_edge(clk_p) then
        d_int(idh downto idl) <= d(idh downto idl);
      end if;
    end process;

  end generate;

  DAQPATH : entity work.emp_daqpath_emp_buf_ex_design
    generic map ( 
      NUM_CHANNELS => N_CHAN,
      NUM_MAIN_REGS => N_MAIN_REGS,
      NUM_CH_REGS => N_CH_REGS,
      NUM_LINKS => N_LINKS
    )
    port map (
      ipb_clk        => clk,
      rx_clk         => clk_p,
      dp_clk         => clk_p,
      tx_clk         => clk_p,
      rst            => rst,
      d              => d_int,
      ipb_in         => ipbw(N_SLV_DAQPATH),
      ipb_out        => ipbr(N_SLV_DAQPATH),
      data_out       => data_out,
      data_we_out    => data_we_out,
      idnw_out       => idnw_out,
      idnw_we_out    => idnw_we_out,
      pause          => backpressure_daqpath,
      slink_data     => slink_data,
      slink_dv       => slink_dv,
      slink_start    => slink_start,
      slink_end      => slink_end
    );
    
  --=========================================================================
  -- Using Slink channel 0 and 1 for daqpath stream (replicated)
  --=========================================================================  
  -- Global id, bx_id, orbit_id counters
  p_slink_trigger_id_data_gen : process (clk)
    begin
      if rising_edge(clk) then
        if (slink_end = '1') then
          ge_id <= ge_id + "1";
          bx_id <= bx_id + "1";
          if ( bx_id = (bx_id'left downto 0 => '1') ) then
            orbit_id <= orbit_id + "1";
          end if;
        end if;
      end if;
    end process;
  
  route_slink_data : for i in 0 to 1 generate
    -- Retrive physics_type and l1a_type from the slink dummy stream
    slink_q(0)(i).trigger_id.physics_type <= slink_i(0).trigger_id.physics_type;
    slink_q(0)(i).trigger_id.l1a_type <= slink_i(0).trigger_id.l1a_type;
    slink_q(0)(i).trigger_id.global_event_id <= ge_id;
    slink_q(0)(i).trigger_id.bx_id <= bx_id;
    slink_q(0)(i).trigger_id.orbit_id <= orbit_id;
    ----------------------------------------------------------------------------  
    -- Data from daqpath
    slink_q(0)(i).daq_data <= slink_data;
    slink_q(0)(i).start <= slink_start;
    slink_q(0)(i).valid <= slink_dv;
    slink_q(0)(i).finish <= slink_end;
    ----------------------------------------------------------------------------
  end generate route_slink_data; 

end rtl;
