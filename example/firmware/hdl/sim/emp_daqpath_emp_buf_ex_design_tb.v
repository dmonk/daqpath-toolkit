// Rev 26/04/2022 TB entity for a N channels daqpath example design with empbutler input, ipbus output

`timescale 1ps/1ps

module emp_daqpath_emp_buf_ex_design_tb ();

  reg           ipb_en,ren;
  reg           clk;
  reg           clk_p;
  reg           rst;
  reg           pause;
  
  wire          global_data_we_out;
  wire   [63:0] global_data_out;
  wire          global_idnw_we_out;
  wire   [31:0] global_idnw_out;
  
  
  initial begin
    clk = 1'b0;
    forever
      clk = #10000 ~clk;
  end

  // clk_p 360MHz  
  initial begin
    clk_p = 1'b0;
    forever
      clk_p = #1350 ~clk_p;
  end
   
  initial begin
    ren = 1'b0; #5000000 ren = 1'b1;
  end
 
  initial begin
    rst = 1'b1; #100000 rst = 1'b0;
  end

  initial begin
    ipb_en = 1'b0; #200000 ipb_en = 1'b1;
  end

  initial begin
    pause = 1'b0;
  end

  wire          ipb_in_strobe;
  wire          ipb_in_write;
  wire   [31:0] ipb_in_addr;
  wire   [31:0] ipb_in_wdata;

  wire          ipb_out_ack;
  wire          ipb_out_err;
  wire   [31:0] ipb_out_rdata;

  wire   [63:0] slink_data;
  wire          slink_dv;
  wire          slink_start;
  wire          slink_end;

  wire   [7:0] ldata_addr;
  wire   [66:0] ldata_data;
 
 emp_daqpath_ipbus_file_reader IPBUS_File_reader (
    .clk (clk), 
    .en (ipb_en), 
    .ipb_in_addr (ipb_in_addr), 
    .ipb_in_wdata (ipb_in_wdata), 
    .ipb_in_strobe (ipb_in_strobe), 
    .ipb_in_write (ipb_in_write) 
  );

 emp_daqpath_ldata_file_reader LDATA_File_reader (
    .clk (clk_p), 
    .en (ipb_en), 
    .ldata_addr (ldata_addr), 
    .ldata_data (ldata_data) 
  );
 
 emp_daqpath_emp_buf_ex_design_wrapper DAQPATH_DUT_WRAPPER(
    .ipb_clk (clk),
    .rx_clk (clk_p),
    .dp_clk (clk_p),
    .tx_clk (clk_p),
    .rst (rst),
    .ldata_addr(ldata_addr),
    .ldata_data(ldata_data),
    .ipb_in_addr(ipb_in_addr),
    .ipb_in_wdata(ipb_in_wdata),
    .ipb_in_strobe(ipb_in_strobe),
    .ipb_in_write(ipb_in_write),
    .ipb_out_rdata(ipb_out_rdata),
    .ipb_out_ack(ipb_out_ack),
    .ipb_out_err(ipb_out_err),
    .pause(pause),    
    .data_out(global_data_out),
    .data_we_out(global_data_we_out),
    .idnw_out(global_idnw_out),
    .idnw_we_out(global_idnw_we_out),
    .slink_data(slink_data),  
    .slink_start(slink_start),
    .slink_dv(slink_dv),
    .slink_end(slink_end)  
  );
 
  emp_daqpath_output_file_writer File_writer(
    .clk (clk_p), 
    .data (global_data_out), 
    .data_dv (global_data_we_out),
    .idnw (global_idnw_out), 
    .idnw_dv (global_idnw_we_out)
  );

  emp_daqpath_ipbus_file_writer IPBUS_File_writer(
    .clk (clk), 
    .data (ipb_out_rdata), 
    .dv (ipb_out_ack & ~ipb_in_write)
  );

endmodule