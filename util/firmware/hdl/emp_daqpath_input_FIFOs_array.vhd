-- Array block of input fifos (Derandomising buffers - 3x channel)
-- Rev 26/04/2022

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.emp_daqpath_types_package.ALL;

entity emp_daqpath_input_FIFOs_array is

  generic ( NUM_CHANNELS : integer := 16);
  
  Port( 
    rx_clk : in std_logic;
    dp_clk : in std_logic;
    rst : in std_logic;
    id_fifo_din : in ctrl_fifo_data_array(NUM_CHANNELS-1 downto 0);
    nw_fifo_din : in ctrl_fifo_data_array(NUM_CHANNELS-1 downto 0);
    word_fifo_din : in data_fifo_data_array(NUM_CHANNELS-1 downto 0);
    id_fifo_dout : out ctrl_fifo_data_array(NUM_CHANNELS-1 downto 0);
    nw_fifo_dout : out ctrl_fifo_data_array(NUM_CHANNELS-1 downto 0);
    word_fifo_dout : out data_fifo_data_array(NUM_CHANNELS-1 downto 0);
    id_fifo_we : in STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    nw_fifo_we : in STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    word_fifo_we : in STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    id_fifo_re : in STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    nw_fifo_re : in STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    word_fifo_re : in STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    id_fifo_empty : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    nw_fifo_empty : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    word_fifo_empty : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    id_fifo_full : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    nw_fifo_full : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0);
    word_fifo_full : out STD_LOGIC_VECTOR(NUM_CHANNELS-1 downto 0)                    
  );
end emp_daqpath_input_FIFOs_array;

architecture rtl of emp_daqpath_input_FIFOs_array is

begin

gen_fifos : for n in NUM_CHANNELS-1 downto 0 generate

id_fifo : entity work.fifo_ctrl_16b_bram
  port map (
    rst => rst,
    wr_clk => rx_clk,
    rd_clk => dp_clk,
    din => id_fifo_din(n),
    wr_en => id_fifo_we(n),
    rd_en => id_fifo_re(n),
    dout => id_fifo_dout(n),
    full => id_fifo_full(n),
    empty => id_fifo_empty(n),
    wr_rst_busy => open,
    rd_rst_busy => open
  );

nw_fifo : entity work.fifo_ctrl_16b_bram
  port map (
    rst => rst,
    wr_clk => rx_clk,
    rd_clk => dp_clk,
    din => nw_fifo_din(n),
    wr_en => nw_fifo_we(n),
    rd_en => nw_fifo_re(n),
    dout => nw_fifo_dout(n),
    full => nw_fifo_full(n),
    empty => nw_fifo_empty(n),
    wr_rst_busy => open,
    rd_rst_busy => open
  );

word_fifo : entity work.fifo_data_64b_bram
  port map (
    rst => rst,
    wr_clk => rx_clk,
    rd_clk => dp_clk,
    din => word_fifo_din(n),
    wr_en => word_fifo_we(n),
    rd_en => word_fifo_re(n),
    dout => word_fifo_dout(n),
    full => word_fifo_full(n),
    empty => word_fifo_empty(n),
    wr_rst_busy => open,
    rd_rst_busy => open
  ); 
    
 end generate gen_fifos;

end rtl;
