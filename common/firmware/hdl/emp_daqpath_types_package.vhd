LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE work.emp_daqpath_user_package.all;

PACKAGE emp_daqpath_types_package IS
  
  type ipbus_data_array is array (natural range <>) of std_logic_vector (31 downto 0);
  type ipbus_data_array2 is array (natural range <>,natural range <>) of std_logic_vector (31 downto 0);  
  type ctrl_fifo_data_array is array (natural range <>) of std_logic_vector (15 downto 0);
  type data_fifo_data32_array is array (natural range <>) of std_logic_vector (31 downto 0);
  type data_fifo_data_array is array (natural range <>) of std_logic_vector (63 downto 0);
  type generic_data_fifo_data_array is array (natural range <>) of std_logic_vector (8*DW_BYTES-1 downto 0);         
   
END; 
