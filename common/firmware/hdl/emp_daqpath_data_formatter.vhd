-- emp_daqpath data formatter block. It interfaces with input daqpath internal output stream (data width permitted : 1,2,4,8 bytes)
-- and provides a formatted 64bit words output stream for the slink interface
-- Rev 07/07/2022

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.emp_daqpath_types_package.ALL;
use work.emp_daqpath_user_package.ALL;

entity emp_daqpath_data_formatter is

  generic( 
    DW_BYTES : positive := 4
  );
    
  Port  (
    clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    ev_id : in STD_LOGIC_VECTOR(15 downto 0);
    start : in STD_LOGIC;
    last : in STD_LOGIC;
    data_in : in STD_LOGIC_VECTOR(8*DW_BYTES-1 downto 0);
    data_we_in : in STD_LOGIC;
    data_out : out STD_LOGIC_VECTOR(63 downto 0);
    data_we_out : out STD_LOGIC;
    idnw_out : out STD_LOGIC_VECTOR(31 downto 0);
    idnw_we_out : out STD_LOGIC    
  );

end emp_daqpath_data_formatter;

architecture emp_daqpath_data_formatter_arch of emp_daqpath_data_formatter is

signal data_out_reg0, data_out_reg1 : std_logic_vector(0 to 63);
signal dv0, dv1, dv1_reg, dv0_reg : std_logic := '0';
signal sel0, sel1, reset0, reset1 : std_logic := '0';
signal idnw_we_int, idnw_we_int_reg : std_logic := '0';

-- Auxiliary signals

signal out_words_cnt, bytes_count : integer := 0;
signal out_words_cnt_en, out_words_cnt_rst, last_reg : std_logic := '0';
signal bytes_count_en, bytes_count_rst : std_logic := '0';
signal ev_id_reg : std_logic_vector(15 downto 0) := (Others => '0');

-- FSM signals
type state_type is (S_IDLE, S_REG0, S_REG1, S_IDNW);
signal cs: state_type;
signal ns: state_type;

begin

-- This simpler version only allows 1,2,4,8 DW bytes
assert ((8 mod DW_BYTES = 0) and DW_BYTES <= 8) report "DW_BYTES allowed values : 1,2,4,8" severity failure;

input_reg_proc : process(clk, rst, start, ev_id, last)
  begin
    if rising_edge(clk) then
      if (start = '1') then
        ev_id_reg <= ev_id;
      end if;
      last_reg <= last;
    end if;
end process;
  
data_out_proc : process(clk, dv0_reg, dv1_reg)
  begin
    if rising_edge(clk) then
      if (dv0_reg = '1') then
        data_out <= data_out_reg0;
        data_we_out <= '1';
      elsif (dv1_reg = '1') then
        data_out <= data_out_reg1;
        data_we_out <= '1';
      else 
        data_out <= (Others => '0');
        data_we_out <= '0';
      end if;
    end if;    
  end process;
  
data_out_reg_proc : process(clk, sel0, sel1, reset0, reset1, bytes_count)
  begin
    if rising_edge(clk) then
    
      if (sel0 = '1') then
        data_out_reg0(bytes_count*8 - DW_BYTES*8 to bytes_count*8-1) <= data_in;
      elsif (reset0 = '1') then
        data_out_reg0 <= (Others => '0');
      end if;
      
      if (sel1 = '1') then
        data_out_reg1(bytes_count*8 - DW_BYTES*8 to bytes_count*8-1) <= data_in;
      elsif (reset1 = '1') then
        data_out_reg1 <= (Others => '0');
      end if;
      
    end if;
  end process;
  
idnw_out_proc : process(clk, rst, ev_id_reg, out_words_cnt, idnw_we_int)
  begin
    if rising_edge(clk) then
      idnw_we_int_reg <= idnw_we_int;
      if (idnw_we_int_reg = '1') then
        idnw_out <= ev_id_reg & STD_LOGIC_VECTOR(to_unsigned(out_words_cnt, 16));
      else
        idnw_out <= (Others => '0');
      end if;
      idnw_we_out <= idnw_we_int_reg;  
    end if;
end process;

  
out_words_cnt_proc: process (out_words_cnt,out_words_cnt_en,out_words_cnt_rst,rst,clk)
  begin
  if (rst = '1') then 
    out_words_cnt <= 0;
  elsif rising_edge(clk) then
    if (out_words_cnt_en = '1') then
      if (out_words_cnt_rst = '1') then
        out_words_cnt <= 0;
      else
        out_words_cnt <= out_words_cnt + 1;
      end if;
    end if;
  end if;          
end process;

bytes_count_proc: process (bytes_count,bytes_count_en,bytes_count_rst,rst,clk)
  begin
  if (rst = '1') then 
    bytes_count <= DW_BYTES;
  elsif rising_edge(clk) then
    if (bytes_count_en = '1') then
      if (bytes_count_rst = '1') then
        bytes_count <= DW_BYTES;
      else
        bytes_count <= (bytes_count + DW_BYTES -1) mod 8 + 1;
      end if;
    end if;
  end if;          
end process;

FSM_regs: process (ns,rst,clk)
  begin
    if (rst = '1') then
      cs <= S_IDLE;
    elsif rising_edge(clk) then
      cs <= ns;
      dv0_reg <= dv0;
      dv1_reg <= dv1;
    end if;
  end process;

FSM_logic: process (start, last, data_we_in, out_words_cnt, bytes_count, cs, dv1_reg, dv0_reg)

  begin
    case cs is
    
        when S_IDLE =>

      out_words_cnt_en <= '1';
      out_words_cnt_rst <= '1';
      bytes_count_en <= '1';
      bytes_count_rst <= '1';
      sel0 <= '0';
      sel1 <= '0';
      reset0 <= '1';
      reset1 <= '1';
      dv0 <= '0';
      dv1 <= '0';
      idnw_we_int <= '0';

      if (start = '1') then
        ns <= S_REG0;
      else
        ns <= S_IDLE;
      end if;

        when S_REG0 =>
      
      bytes_count_rst <= '0';  
      out_words_cnt_rst <= '0';
      sel1 <= '0';
      reset0 <= '0';
      reset1 <= '1';
      dv1 <= '0';
      idnw_we_int <= '0';
        
      if (data_we_in = '1') then
        bytes_count_en <= '1';
        sel0 <= '1';
      else
        bytes_count_en <= '0';
        sel0 <= '0';
      end if;
      
--      if(bytes_count = 8) then
--        if(last = '1') then
--          dv0 <= '1';
--          out_words_cnt_en <= '1';
--          ns <= S_IDNW;
--        elsif (last = '0' and data_we_in = '1') then
--          dv0 <= '1';
--          out_words_cnt_en <= '1';
--          ns <= S_REG1;
--        else
--          dv0 <= '0';
--          out_words_cnt_en <= '0';
--          ns <= S_REG0;
--        end if;
--      else
--        if(last = '1') then
--          if (bytes_count /= DW_BYTES and data_we_in = '0') then
--            dv0 <= '1';
--            out_words_cnt_en <= '1';
--            ns <= S_IDNW;
--          elsif (bytes_count = DW_BYTES and data_we_in = '0') then
--            dv0 <= '0';
--            out_words_cnt_en <= '0';
--            ns <= S_IDNW;
--          elsif (data_we_in = '1') then
--            dv0 <= '1';
--            out_words_cnt_en <= '1';
--            ns <= S_IDNW;
--          end if;
--        else
--          dv0 <= '0';
--          out_words_cnt_en <= '0';
--          ns <= S_REG0;
--        end if;
--      end if;

      if(last = '1') then
        if (data_we_in = '0') then
          if (bytes_count /= DW_BYTES) then
            dv0 <= '1';
            out_words_cnt_en <= '1';
            ns <= S_IDNW;                                           
          else
            dv0 <= '0';
            out_words_cnt_en <= '0';
            ns <= S_IDNW;
          end if;
        else
          dv0 <= '1';
          out_words_cnt_en <= '1';
          ns <= S_IDNW;
        end if;
      else
        if (data_we_in = '0') then
          dv0 <= '0';
          out_words_cnt_en <= '0';
          ns <= S_REG0;                                           
        else
          if (bytes_count = 8) then
            dv0 <= '1';
            out_words_cnt_en <= '1';
            ns <= S_REG1;
          else
            dv0 <= '0';
            out_words_cnt_en <= '0';
            ns <= S_REG0;
          end if;
        end if;
      end if;       
       
      
        when S_REG1 =>
      
      bytes_count_rst <= '0';     
      out_words_cnt_rst <= '0';
      out_words_cnt_en <= '0';
      sel0 <= '0';      
      reset0 <= '1';
      reset1 <= '0';
      dv0 <= '0';
      idnw_we_int <= '0';
        
      if (data_we_in = '1') then
        bytes_count_en <= '1';
        sel1 <= '1';
      else
        bytes_count_en <= '0';
        sel1 <= '0';
      end if;
      
--      if(bytes_count = 8) then
--        if(last = '1') then
--          ns <= S_IDNW;
--          dv1 <= '1';
--          out_words_cnt_en <= '1';
--        elsif (last = '0' and data_we_in = '1') then
--          ns <= S_REG0;
--          dv1 <= '1';
--          out_words_cnt_en <= '1';
--        else
--          ns <= S_REG1;
--          dv1 <= '0';
--          out_words_cnt_en <= '0';
--        end if;
--      else
--        if(last = '1') then
--          if (bytes_count /= DW_BYTES and data_we_in = '0') then
--            dv1 <= '1';
--            out_words_cnt_en <= '1';
--            ns <= S_IDNW;
--          elsif (bytes_count = DW_BYTES and data_we_in = '0') then
--            dv1 <= '0';
--            out_words_cnt_en <= '0';
--            ns <= S_IDNW;
--          elsif (data_we_in = '1') then
--            dv1 <= '1';
--            out_words_cnt_en <= '1';
--            ns <= S_IDNW;
--          end if;
--        else
--          dv1 <= '0';
--          out_words_cnt_en <= '0';
--          ns <= S_REG1;
--        end if; 
--      end if;

     if(last = '1') then
        if (data_we_in = '0') then
          if (bytes_count /= DW_BYTES) then
            dv1 <= '1';
            out_words_cnt_en <= '1';
            ns <= S_IDNW;                                           
          else
            dv1 <= '0';
            out_words_cnt_en <= '0';
            ns <= S_IDNW;
          end if;
        else
          dv1 <= '1';
          out_words_cnt_en <= '1';
          ns <= S_IDNW;
        end if;
      else
        if (data_we_in = '0') then
          dv1 <= '0';
          out_words_cnt_en <= '0';
          ns <= S_REG1;                                           
        else
          if (bytes_count = 8) then
            dv1 <= '1';
            out_words_cnt_en <= '1';
            ns <= S_REG0;
          else
            dv1 <= '0';
            out_words_cnt_en <= '0';
            ns <= S_REG1;
          end if;
        end if;
      end if;           
      
        when S_IDNW =>
          
      out_words_cnt_rst <= '0';
      out_words_cnt_en <= '0';
      bytes_count_rst <= '0';
      bytes_count_en <= '0'; 
      sel0 <= '0';
      sel1 <= '0';
      reset0 <= '0';
      reset1 <= '0';
      dv0 <= '0';
      dv1 <= '0';
      idnw_we_int <= '1';
        
      ns <= S_IDLE; 
      
      when Others =>
      
      bytes_count_rst <= '0';
      bytes_count_en <= '0';
      out_words_cnt_en <= '1';
      out_words_cnt_rst <= '1';
      sel0 <= '0';
      sel1 <= '0';
      reset0 <= '1';
      reset1 <= '1';
      dv0 <= '0';
      dv1 <= '0';
      idnw_we_int <= '0';
      
      ns <= S_IDLE;

    end case;

  end process; 

end emp_daqpath_data_formatter_arch;