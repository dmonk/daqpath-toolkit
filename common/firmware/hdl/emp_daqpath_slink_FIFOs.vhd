-- Block containing the output FIFOs to store the output stream and the SLINK formatter/sender FSM which provides data in the correct format for SLINK-ROCKET interface
-- Rev - 26/04/2022

library IEEE;
Library xpm;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use xpm.vcomponents.all;

entity emp_daqpath_slink_FIFOs is
    generic (Data_fifo_word_depth : integer := 32768);
	port(
		clk: in std_logic;
		en: in std_logic;
		reset: in std_logic;
		idnw_fifo_wdata: in std_logic_vector(31 downto 0);
		idnw_fifo_we: in std_logic;
		idnw_fifo_empty: out std_logic;
		idnw_fifo_full: out std_logic;
		dw_fifo_wdata: in std_logic_vector(63 downto 0);
		dw_fifo_we: in std_logic;
		dw_fifo_empty: out std_logic;
		dw_fifo_full: out std_logic;
		pause : in std_logic;
		slink_data: out std_logic_vector(63 downto 0);
		slink_dv: out std_logic;
		slink_start: out std_logic;
		slink_end: out std_logic
	);
	
end emp_daqpath_slink_FIFOs;

architecture rtl of emp_daqpath_slink_FIFOs is

	signal wr_clk, rd_clk : std_logic;

	signal idnw_fifo_re, dw_fifo_re : std_logic;
	signal idnw_fifo_rdata : std_logic_vector(31 downto 0);
	signal dw_fifo_rdata : std_logic_vector(63 downto 0);
	signal int_idnw_fifo_empty : std_logic;

begin
    
--idnw_fifo : entity work.fifo_data_32b_bram
--    port map (
--      rst => reset,
--      wr_clk => clk,
--      rd_clk => clk,
--      din => idnw_fifo_wdata,
--      wr_en => idnw_fifo_we,
--      rd_en => idnw_fifo_re,
--      dout => idnw_fifo_rdata,
--      full => idnw_fifo_full,
--      empty => int_idnw_fifo_empty,
--      wr_rst_busy => open,
--      rd_rst_busy => open
--    );	

--idnw_fifo_empty <= int_idnw_fifo_empty;

----dw_fifo : entity work.fifo_data_64b_bram
--dw_fifo : entity work.fifo_data_32k64b_bram
--    port map (
--      rst => reset,
--      wr_clk => clk,
--      rd_clk => clk,
--      din => dw_fifo_wdata,
--      wr_en => dw_fifo_we,
--      rd_en => dw_fifo_re,
--      dout => dw_fifo_rdata,
--      full => dw_fifo_full,
--      empty => dw_fifo_empty,
--      wr_rst_busy => open,
--      rd_rst_busy => open
--    );	

dw_fifo : xpm_fifo_async
    generic map (
      CDC_SYNC_STAGES => 2, -- DECIMAL
      DOUT_RESET_VALUE => "0", -- String
      ECC_MODE => "no_ecc", -- String
      FIFO_MEMORY_TYPE => "block", -- String
      FIFO_READ_LATENCY => 0, -- DECIMAL  --first word fall through
      FIFO_WRITE_DEPTH => Data_fifo_word_depth, -- DECIMAL
      FULL_RESET_VALUE => 0, -- DECIMAL
      PROG_EMPTY_THRESH => 5, -- DECIMAL
      PROG_FULL_THRESH => 5, -- DECIMAL
      RD_DATA_COUNT_WIDTH => 1, -- DECIMAL
      READ_DATA_WIDTH => 64, -- DECIMAL
      READ_MODE => "fwft", -- String
      RELATED_CLOCKS => 0, -- DECIMAL
      USE_ADV_FEATURES => "0000", -- String
      WAKEUP_TIME => 0, -- DECIMAL
      WRITE_DATA_WIDTH => 64, -- DECIMAL
      WR_DATA_COUNT_WIDTH => 1 -- DECIMAL
     )
     port map (
       rst => reset,
       wr_clk => clk,
       rd_clk => clk,
       din => dw_fifo_wdata,
       wr_en => dw_fifo_we,
       rd_en => dw_fifo_re,
       dout => dw_fifo_rdata,
       full => dw_fifo_full,
       empty => dw_fifo_empty,
       sleep => '0',
       injectdbiterr => '0',
       injectsbiterr => '0',
       wr_rst_busy => open,
       rd_rst_busy => open
     );
     
idnw_fifo : xpm_fifo_async
    generic map (
      CDC_SYNC_STAGES => 2, -- DECIMAL
      DOUT_RESET_VALUE => "0", -- String
      ECC_MODE => "no_ecc", -- String
      FIFO_MEMORY_TYPE => "block", -- String
      FIFO_READ_LATENCY => 0, -- DECIMAL  --first word fall through
      FIFO_WRITE_DEPTH => 1024, -- DECIMAL
      FULL_RESET_VALUE => 0, -- DECIMAL
      PROG_EMPTY_THRESH => 5, -- DECIMAL
      PROG_FULL_THRESH => 5, -- DECIMAL
      RD_DATA_COUNT_WIDTH => 1, -- DECIMAL
      READ_DATA_WIDTH => 32, -- DECIMAL
      READ_MODE => "fwft", -- String
      RELATED_CLOCKS => 0, -- DECIMAL
      USE_ADV_FEATURES => "0000", -- String
      WAKEUP_TIME => 0, -- DECIMAL
      WRITE_DATA_WIDTH => 32, -- DECIMAL
      WR_DATA_COUNT_WIDTH => 1 -- DECIMAL
     )
     port map (
       rst => reset,
       wr_clk => clk,
       rd_clk => clk,
       din => idnw_fifo_wdata,
       wr_en => idnw_fifo_we,
       rd_en => idnw_fifo_re,
       dout => idnw_fifo_rdata,
       full => idnw_fifo_full,
       empty => int_idnw_fifo_empty,
       sleep => '0',
       injectdbiterr => '0',
       injectsbiterr => '0',
       wr_rst_busy => open,
       rd_rst_busy => open
     );  	

slink_data <= dw_fifo_rdata;

fifo_fsm : entity work.emp_daqpath_slink_transmitter_fsm
    port map (
      clk => clk,
      rst => reset,
      en => en,
      pause => pause,
      empty => int_idnw_fifo_empty,
      slink_rdata => dw_fifo_rdata,
      idnw_rdata => idnw_fifo_rdata,
      slink_re => dw_fifo_re,
      slink_stream_dv => slink_dv,
      slink_stream_start => slink_start,
      slink_stream_end => slink_end,
      idnw_re => idnw_fifo_re
    );

end rtl;
