-- FSM for data transfer from IPBUS input fifos to input fifos at the correct timestamp
-- Rev - 26/04/2022

library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;


entity emp_daqpath_ipbus_fsm is

  port (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    en : IN STD_LOGIC;
    empty : in STD_LOGIC;
    ts : IN STD_LOGIC_VECTOR(31 downto 0);
    nw : IN STD_LOGIC_VECTOR(15 downto 0);
    ts_fifo_re : OUT STD_LOGIC;
    id_fifo_re : OUT STD_LOGIC;
    id_fifo_we : OUT STD_LOGIC;
    nw_fifo_re : OUT STD_LOGIC;
    nw_fifo_we : OUT STD_LOGIC;
    data_fifo_re : OUT STD_LOGIC;
    data_fifo_we : OUT STD_LOGIC
  );

end emp_daqpath_ipbus_fsm;


ARCHITECTURE emp_daqpath_ipbus_fsm of emp_daqpath_ipbus_fsm is

type state_type is (S_IDLE, S_READ, S_LAST_READ, S_TS_NW_1, S_TS_NW_2, S_STOP);
signal cs: state_type;
signal ns: state_type;

signal cnt , nw_next_int, nw_curr_int: integer;
signal ts_curr_int, ts_next_int: integer;
signal ts_cnt : natural := 0;
signal cnt_en, cnt_rst, ts_fifo_reg_en, nw_fifo_reg_en : std_logic;

signal ts_next : std_logic_vector(31 downto 0);
signal ts_curr : std_logic_vector(31 downto 0) := (others => '0');
signal nw_next : std_logic_vector(15 downto 0);
signal nw_curr : std_logic_vector(15 downto 0) := (others => '0');

begin

  ts_next <= ts;
  nw_next <= nw;

  ts_curr_process : process(clk, ts_fifo_reg_en, rst, ts_next)
  begin
    if rising_edge(clk) then
      if(rst = '1') then
        ts_curr <= (others => '0');
      elsif (ts_fifo_reg_en = '1') then
        ts_curr <= ts_next;
      end if;
    end if;
  end process;
  
   nw_reg_process : process(clk, nw_next, nw_fifo_reg_en, rst)
  begin
    if rising_edge(clk) then
      if(rst = '1') then
        nw_curr <= (others => '0');
      elsif (nw_fifo_reg_en = '1') then
        nw_curr <= nw_next;
      end if;
    end if;
  end process;

  cnt_proc: process (cnt,cnt_en,cnt_rst,rst,clk)
    begin
      if (rst = '1') then 
        cnt <= 1;
      elsif (cnt_en = '1') and rising_edge(clk) then
        if (cnt_rst = '1') then
          cnt <= 1;
        else
          cnt <= cnt + 1;
        end if;
      end if;        
  end process;

  ts_cnt_proc: process (ts_cnt,en,rst,clk)
    begin
    if (rst = '1') then 
      ts_cnt <= 0;
    elsif (en = '1') and rising_edge(clk) then
      ts_cnt <= ts_cnt + 1;
    end if; 
  end process;

  FSM_regs: process (ns,rst,clk)
    begin
      if (rst = '1') then
        cs <= S_IDLE;
      elsif rising_edge(clk) then
        cs <= ns;
      end if;
  end process;
  
  
  nw_next_int <= TO_INTEGER(unsigned(nw_next));
  ts_next_int <= TO_INTEGER(unsigned(ts_next));
  nw_curr_int <= TO_INTEGER(unsigned(nw_curr));
  ts_curr_int <= TO_INTEGER(unsigned(ts_curr));

  FSM_logic: process (en,cnt,ts_cnt,ts_curr_int,ts_next_int,nw_next_int,nw_curr_int,cs)
    begin
      case cs is
    
        when S_IDLE => 

      cnt_en <= '0';
      cnt_rst <= '0';
      data_fifo_re <= '0';
      data_fifo_we <= '0';
      ts_fifo_re <= '0';
      id_fifo_re <= '0';
      id_fifo_we <= '0';
      nw_fifo_re <= '0';
      nw_fifo_we <= '0';
      nw_fifo_reg_en <= '0';
      ts_fifo_reg_en <= '0';

      -- IPBUS FIFOs are surely not empty when en goes to '1'
      if (en = '1' and empty = '0') then
        if (ts_cnt = 1) then
            ns <= S_TS_NW_1;
        elsif (ts_cnt = integer'high) then
            ns <= S_STOP;
        -- ts_curr is at minimum 50 the first time, so no problems
        elsif (ts_cnt = ts_curr_int - 1) then
          if (nw_curr_int = 1) then
            ns <= S_LAST_READ;
          else
            ns <= S_READ;
          end if;
        else
          ns <= S_IDLE;
        end if;
      else
        ns <= S_IDLE;
      end if;  

        when S_READ =>
  
      cnt_en <= '1';
      cnt_rst <= '0';
      data_fifo_re <= '1';
      data_fifo_we <= '1';
      ts_fifo_re <= '0';
      id_fifo_re <= '0';
      id_fifo_we <= '0';
      nw_fifo_re <= '0';
      nw_fifo_we <= '0';
      nw_fifo_reg_en <= '0';
      ts_fifo_reg_en <= '0';

      if (cnt = nw_curr_int -1) then    
        ns <= S_LAST_READ;
      else
        ns <= S_READ;
      end if; 
    
        when S_LAST_READ =>

      cnt_en <= '1';
      cnt_rst <= '1';
      data_fifo_re <= '1';
      data_fifo_we <= '1';
      ts_fifo_re <= '1';
      id_fifo_re <= '1';
      id_fifo_we <= '1';
      nw_fifo_re <= '1';
      nw_fifo_we <= '1';
      nw_fifo_reg_en <= '1';
      ts_fifo_reg_en <= '1';

      if (ts_cnt = ts_next_int - 1) then
        if (nw_next_int = 1) then
          ns <= S_LAST_READ;
        else
          ns <= S_READ;
        end if;
      else
        ns <= S_IDLE;
      end if;
      
        when S_TS_NW_1 =>

      cnt_en <= '0';
      cnt_rst <= '0';
      data_fifo_re <= '0';
      data_fifo_we <= '0';
      ts_fifo_re <= '0';
      id_fifo_re <= '0';
      id_fifo_we <= '0';
      nw_fifo_re <= '0';
      nw_fifo_we <= '0';
      nw_fifo_reg_en <= '1';
      ts_fifo_reg_en <= '1';

      ns <= S_TS_NW_2;
      
        when S_TS_NW_2 =>

      cnt_en <= '0';
      cnt_rst <= '0';
      data_fifo_re <= '0';
      data_fifo_we <= '0';
      ts_fifo_re <= '1';
      id_fifo_re <= '0';
      id_fifo_we <= '0';
      nw_fifo_re <= '1';
      nw_fifo_we <= '0';
      nw_fifo_reg_en <= '0';
      ts_fifo_reg_en <= '0';

      ns <= S_IDLE;

        when S_STOP =>  

      cnt_en <= '0';
      cnt_rst <= '0';
      ts_fifo_re <= '0';
      id_fifo_re <= '0';
      id_fifo_we <= '0';
      nw_fifo_re <= '0';
      nw_fifo_we <= '0';
      data_fifo_re <= '0';
      data_fifo_we <= '0';
      nw_fifo_reg_en <= '0';
      ts_fifo_reg_en <= '0';

      ns <= S_STOP;
      
        when others =>  

      cnt_en <= '0';
      cnt_rst <= '0';
      ts_fifo_re <= '0';
      id_fifo_re <= '0';
      id_fifo_we <= '0';
      nw_fifo_re <= '0';
      nw_fifo_we <= '0';
      data_fifo_re <= '0';
      data_fifo_we <= '0';
      nw_fifo_reg_en <= '0';
      ts_fifo_reg_en <= '0';

      ns <= S_IDLE;      
    
      end case;

  end process;
      
end emp_daqpath_ipbus_fsm;
