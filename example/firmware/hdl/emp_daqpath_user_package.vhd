LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

PACKAGE emp_daqpath_user_package IS
  
  --======================================================
  -- DAQPATH example designs settings

  constant N_CHAN : integer := 16;                   --total number of channnels
  constant N_CHAN_PER_GROUP : integer := 4;          --number of channels per pipeline group (number of group is then = ceil(N_CHAN/N_CHAN_PER_GROUP))
  constant DW_BYTES : integer := 8;
  
  --======================================================     
   
END; 
