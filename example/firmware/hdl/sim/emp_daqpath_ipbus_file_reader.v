// Rev 26/04/2022 - Input txt file reader entity

`timescale 1ns / 1ps

module emp_daqpath_ipbus_file_reader(clk, en, ipb_in_addr, ipb_in_wdata, ipb_in_strobe, ipb_in_write);

input wire clk;
input wire en;

output reg [31:0] ipb_in_addr;
output reg [31:0] ipb_in_wdata;
output reg ipb_in_strobe;
output reg  ipb_in_write;

reg [31:0] ts_cnt;
reg [31:0] ts_in;
reg ack;

reg strobe_in;
reg write_in;
reg [31:0] addr_in, addr_in_save;
reg [31:0] wdata_in, wdata_in_save;

reg event_rd;

integer infile, r;

initial #0.001
  begin
    ts_cnt = 32'h00000000;
    ipb_in_strobe = 1'b0;
    ipb_in_write = 1'b0;
    ipb_in_addr = 32'b0;
    ipb_in_wdata = 32'b0;
    ack = 1'b0;
  end

always @(posedge clk) #0.001
  if (en == 1'b1)
  ts_cnt = ts_cnt + 1'b1;

always @(posedge clk) #0.001
  if (en == 1'b1) 
    begin
      if (ts_cnt < ts_in)
        begin
          if(ack == 1'b1 && ipb_in_write == 1'b1 && (addr_in_save[31:8] == 24'h400000 || addr_in_save[31:12] == 20'h40100))
            begin
              ipb_in_strobe = 1'b1;
            ipb_in_write = 1'b1;
              ipb_in_addr = addr_in_save;
              ipb_in_wdata = wdata_in_save;
              ack = 1'b0;             
            end
          else
            begin
            ipb_in_strobe = 1'b0;
            ipb_in_write = 1'b0;
              ipb_in_addr = 32'b0;
              ipb_in_wdata = 32'b0;
              ack = 1'b0;
            end
        end
      else
        begin
        ipb_in_strobe = 1'b1;
        ipb_in_write = write_in;
          ipb_in_addr = addr_in;
          addr_in_save = addr_in;
          ipb_in_wdata = wdata_in;
          wdata_in_save = wdata_in;
          ack = 1'b1;
        end
    end

always #0.001
  if (ts_cnt == ts_in)
  begin 
    event_rd = 1'b1; #0.001 event_rd = 1'b0; 
  end
  else
    event_rd = 1'b0;

initial #0.001
  begin
    infile=$fopen("Control/main.txt","r");
  end

initial #0.001
  begin
    r = $fscanf(infile,"%h %h %h %h %h\n",ts_in,strobe_in,write_in,addr_in,wdata_in);
    while (!$feof(infile))
      begin
        @(posedge event_rd) #0.001
        r = $fscanf(infile,"%h %h %h %h %h\n",ts_in,strobe_in,write_in,addr_in,wdata_in);
      end
    $fclose(infile);
  end
 
endmodule