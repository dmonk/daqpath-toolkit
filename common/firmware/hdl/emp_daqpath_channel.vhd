-- Channel block (Pipelind version) - Containing channel FSM and data multiplexing logic  
-- Rev - 26/04/2022

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
library work;
 
entity emp_daqpath_channel is

  generic(
    CHANNEL_NUM : integer := 0;
    DW_BYTES : positive := 4;
    PIPELINE_STAGES : natural := 0;      --number of pipeline registers required to respect logic
    OUTPUT_REG : boolean := false        --true if output register is present
  );
    
  port (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    en : IN STD_LOGIC;
    hdr_en : IN STD_LOGIC;
    mask : IN STD_LOGIC;
    token_in : IN STD_LOGIC;
    we_in : IN STD_LOGIC;
    data_in : IN STD_LOGIC_VECTOR(8*DW_BYTES-1 downto 0);
    current_id : IN STD_LOGIC_VECTOR(15 downto 0);
    token_out : OUT STD_LOGIC;
    data_out : OUT STD_LOGIC_VECTOR(8*DW_BYTES-1 downto 0);
    we_out : OUT STD_LOGIC;
    id_fifo_re : OUT STD_LOGIC;
    id_fifo_rdata : IN STD_LOGIC_VECTOR(15 downto 0);
    nw_fifo_re : OUT STD_LOGIC;
    nw_fifo_rdata : IN STD_LOGIC_VECTOR(15 downto 0);
    data_fifo_re : OUT STD_LOGIC;
    word_fifo_rdata : IN STD_LOGIC_VECTOR(8*DW_BYTES-1 downto 0)
  );

end emp_daqpath_channel;

ARCHITECTURE emp_daqpath_channel_arch of emp_daqpath_channel is

-- Channel_FSM signals

signal header , error : STD_LOGIC_VECTOR(8*DW_BYTES-1 downto 0);
signal header_high, header_low , error_high, error_low : STD_LOGIC_VECTOR(7 downto 0);
signal data_fifo_we : STD_LOGIC;
signal int_data_fifo_we : STD_LOGIC;
signal reg_data_fifo_we : STD_LOGIC;
signal hdr_sel, err_sel : STD_LOGIC_VECTOR(1 downto 0);
signal data_sel : STD_LOGIC;

-- Other signals

signal channel : STD_LOGIC_VECTOR(7 downto 0); 

signal int_data_out : STD_LOGIC_VECTOR(8*DW_BYTES-1 downto 0);
signal reg_data_out : STD_LOGIC_VECTOR(8*DW_BYTES-1 downto 0);

type pipeline_data is array (0 to PIPELINE_STAGES) of STD_LOGIC_VECTOR(8*DW_BYTES + 22 -1 downto 0);
signal reg_data : pipeline_data;

begin

reg_data(0) <= nw_fifo_rdata & data_fifo_we & hdr_sel & data_sel & err_sel & word_fifo_rdata;

gen_pipeline : 
for n in 1 to PIPELINE_STAGES generate

  Stagex : entity work.emp_daqpath_pipeline_stage  
    generic map (
      NBIT => (8*DW_BYTES + 6 + 16)
    )
    port map (
      clk => clk,
      rst => rst,
      din => reg_data(n-1) ,
      dout => reg_data(n)
    );

end generate gen_pipeline;

channel <= std_logic_vector(to_unsigned(CHANNEL_NUM, channel'length));

header_error_proc : process(channel, reg_data, header_high, header_low, error_high, error_low)
begin
  if(DW_BYTES >2) then
    header(8*DW_BYTES-1 downto 16) <= (Others=>'1');
    error(8*DW_BYTES-1 downto 16) <= (Others=>'1');
    header(15 downto 0) <= header_high & header_low;
    error(15 downto 0) <= error_high & error_low;
  end if;
  if(DW_BYTES = 2) then
    header(15 downto 0) <= header_high & header_low;
    error(15 downto 0) <= error_high & error_low;
  end if;
  header_high <= channel;
  header_low <= reg_data(PIPELINE_STAGES)(8*DW_BYTES+13 downto 8*DW_BYTES+6);
  error_high <= channel;
  error_low <= (Others => '1');
end process;

int_data_proc : process(reg_data, data_in, error, header, header_high, header_low, error_high, error_low)
begin
  if (reg_data(PIPELINE_STAGES)(8*DW_BYTES+2) = '1') then
    int_data_out <= reg_data(PIPELINE_STAGES)(8*DW_BYTES-1 downto 0);
  elsif (reg_data(PIPELINE_STAGES)(8*DW_BYTES+4 downto 8*DW_BYTES+3) = "11") then
    int_data_out <= header;
  elsif (reg_data(PIPELINE_STAGES)(8*DW_BYTES+4 downto 8*DW_BYTES+3) = "01") then
    int_data_out(7 downto 0) <= header_high;
  elsif (reg_data(PIPELINE_STAGES)(8*DW_BYTES+4 downto 8*DW_BYTES+3) = "10") then
    int_data_out(7 downto 0) <= header_low;
  elsif (reg_data(PIPELINE_STAGES)(8*DW_BYTES+1 downto 8*DW_BYTES) = "11") then
    int_data_out <= error;
  elsif (reg_data(PIPELINE_STAGES)(8*DW_BYTES+1 downto 8*DW_BYTES) = "01") then
    int_data_out(7 downto 0) <= error_high;
  elsif (reg_data(PIPELINE_STAGES)(8*DW_BYTES+1 downto 8*DW_BYTES) = "10") then
    int_data_out(7 downto 0) <= error_low;
  else
    int_data_out <= data_in;
  end if;    
  
  int_data_fifo_we <= reg_data(PIPELINE_STAGES)(8*DW_BYTES+4) or reg_data(PIPELINE_STAGES)(8*DW_BYTES+3) or reg_data(PIPELINE_STAGES)(8*DW_BYTES+2) or reg_data(PIPELINE_STAGES)(8*DW_BYTES+1) or reg_data(PIPELINE_STAGES)(8*DW_BYTES);
end process;

oreg_proc : process (clk,int_data_out,int_data_fifo_we, we_in)
  begin
    
    if rising_edge(clk) then
      reg_data_out <= int_data_out;
      reg_data_fifo_we <= int_data_fifo_we or we_in;
    end if;
     
 end process;

data_out <= reg_data_out when (OUTPUT_REG = TRUE) else int_data_out;
we_out <= reg_data_fifo_we when (OUTPUT_REG = true) else (we_in or int_data_fifo_we);

fsm : entity work.emp_daqpath_channel_fsm
  port map (
    clk => clk,
    rst => rst,
    token_in => token_in,
    hdr_en => hdr_en,
    mask => mask,
    nw => nw_fifo_rdata,
    ev_id => id_fifo_rdata,
    current_id => current_id,
    token_out => token_out,
    id_fifo_re => id_fifo_re,
    nw_fifo_re => nw_fifo_re,
    data_fifo_re => data_fifo_re,
    data_fifo_we => data_fifo_we,
    data_sel => data_sel,
    hdr_sel => hdr_sel,
    err_sel => err_sel
 );

end emp_daqpath_channel_arch;