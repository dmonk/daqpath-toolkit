// Rev 26/04/2022 - Input txt ldata file reader entity

`timescale 1ns / 1ps
module emp_daqpath_ldata_file_reader(clk, en, ldata_addr, ldata_data);

input wire clk;
input wire en;

output reg [7:0] ldata_addr;
output reg [66:0] ldata_data;

reg [31:0] ts_cnt;
reg [31:0] ts_in;
reg ack;

reg [7:0] addr_in;
reg [66:0] data_in;

reg event_rd;

integer infile, r;

initial #0.001
  begin
    ts_cnt = 32'h00000000;
  end

always @(posedge clk) #0.001
  if (en == 1'b1)
  ts_cnt = ts_cnt + 1'b1;

always #0.001
  if (ts_cnt == ts_in)
  begin 
    event_rd = 1'b1; #0.001 event_rd = 1'b0; 
  end
  else
    event_rd = 1'b0;

initial #0.001
  begin
    infile=$fopen("Control/ldata.txt","r");
  end

initial #0.001
  begin
    r = $fscanf(infile,"%h %h %h\n",ts_in,addr_in,data_in);
    while (!$feof(infile))
      begin
        @(posedge event_rd) #0.001
        r = $fscanf(infile,"%h %h %h\n",ts_in,addr_in,data_in);
      end
    $fclose(infile);
  end

always @(posedge clk) #0.001
  if (ts_cnt == ts_in)
    begin
      ldata_addr = addr_in;
      ldata_data = data_in;
    end
  else
    begin
      ldata_addr = 0;
      ldata_data = 67'b0;
    end
 
endmodule