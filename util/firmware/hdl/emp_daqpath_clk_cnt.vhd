-- Generic clock counter for debugging during HW tests
-- Rev. 26/04/2022

library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity emp_daqpath_clk_cnt is
  generic (
    NB : integer range 1 to 32 := 32 -- Number of bits
    );  
  port
    (clk : in std_logic;
     en : in std_logic; 
     rst : in std_logic; 
     cnt_out : out std_logic_vector(31 downto 0)
    );

end emp_daqpath_clk_cnt;

architecture emp_daqpath_clk_cnt_arch of emp_daqpath_clk_cnt is

signal cnt_data : std_logic_vector(31 downto 0);

begin
  
cnt_proc : process (clk,rst,en,cnt_data)
  
  begin
    
    if (rst = '1') then
        cnt_data <= (others => '0');
    elsif rising_edge(clk) and (en = '1') then
        cnt_data <= cnt_data + '1';
    end if;
    
    cnt_out <= cnt_data(31 downto 0);
     
  end process;
  
end emp_daqpath_clk_cnt_arch;
